# TAUMATA... #

Taumata... est un logiciel permettant d'envoyer et recevoir des fichiers dans les HOMEDIRs des utilisateurs dans un LDAP.

Il est pensé pour permettre aux professeurs de distribuer des devoirs à ses élèves et ramasser le travail réalisé.

Les fichiers executables sont dans le dossier BINARIES