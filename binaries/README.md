## CHANGELOG ##

version 22/01/2019
* New : Voir le contenu de son dossier perso chez l'élève
* Fix : Les liens pointaient vers une vieille version du dépot
* Fix : L'envoi d'un dossier à partir du homedir ne marchait pas

version 11/01/2019
* New : Utilisation de la date de modification plutot que de création pour le ramassage des fichiers
* New : Créer un sous dossier par classe
* New : Envoyer des raccourcis (.lnk) et des liens vers internet (.url)
* New : Ajoute le support pour le drag&drop
* Fix : Le bouton "Ramasser" ne reste plus grisé après un clic sans utilisateur sélectionné

version 13/10/2017
* New : Filtre sur les groupes dans BAWG
* New : Personner le dossier d'envoi (plus forcément "devoirs")

version 02/05/2017
* Fix : Erreur sur le label "liste blanche" dans BAWG
* Fix : Une liste noire vide excluait tous les utilisateurs
* New : Log de la partie import si debug mode est coché dans Cryptonic (attention, génération plus lente du fichier de cache)

version 30/03/2017
* Fix : La liste blanche s'applique désormais sur le nom du groupe et plus sur l'utilisateur

version 16/03/2017
* Fix : La liste blanche appliquait un filtrage trop restrictif
* New : Nouvel outil pour créer et modifier les listes noire & blanche : BAWG.exe

version 03/03/2017
* New : La liste blanche peut comporter plusieurs valeurs (un filtre par ligne)
* New : La liste blanche et la liste noire sont séparées sur 2 fichiers (respectivement liste_blanche.txt et liste_noire.txt). Attention, le filtre est sensible à la casse.
* New : Ecran pour signifier à l'utilisateur que le fichier LDAP.json (cache) est en cours de création (on ne savait pas du tout si Taumata était lancé ou pas).
* Fix : La liste noire est appliquée sur le fichier LDAP.json et plus seulement à l'affichage.
* Fix : Un nom de groupe pouvait être vide.
* Fix : Un nom de groupe pouvait déjà exister.
* Fix : Un utilisateur dans un groupe qui n'existait plus dans la liste plantait le soft.

version 05/10/2016

* Fix : Les groupes vides étaient toujours visibles même si l'option était cochée.

version 23/09/2016

* New : Possibilité de masquer les utilisateurs désactivés (réglage dans Cryptonic)
* New : Possibilité de n'importer qu'un groupe particulier (réglage dans Cryptonic)
* Fix : Les noms s'affichent correctement (plus le login)

version 13/09/2016

* New : Possibilité d'ajouter des dossiers complets à la copie

version 06/09/2016

* Fix : Bouton "distribuer" est désactivé si aucun user sélectionné.
* New : Limite de la taille de l'envoi des fichiers (réglage dans Cryptonic).
* New : Changement de l'icone de Cryptonic.
* Fix : Si l'option est cochée, les groupes vides n'apparaissent plus.


version 02/06/2016

* Fix du fichier de cache
* Fix des problèmes de groupes (liés au cache)


version 26/05/2016

* Multithreading pour la copie des fichiers (le programme ne donne plus l'impression de figer)
* Barre de progression
* Icone plus lisible sur fond sombre


version 19/04/2016

* Les groupes ne pouvaient pas être chargés après une sauvegarde


version 24/03/2016

* Possibilité de sauvegarder des sélections d'utilisateurs


version 15/03/2016

* Copie récursive pour le ramassage
* Création d'un sous dossier par utilisateur coché par défaut
