﻿/*
 * TAUMATA
 * 
 *  BLACK AND WHITE GENERATOR (BAWG)
 *  DAVID LE VIAVANT 2017
 *  LICENCE GPL3
 * 
 * */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;

namespace BAWG
{
    public partial class Form1 : Form
    {

        // passphrase to decode config file
        string PasswordHash = "Al3x1s,C'e$tM0nPet!tP0t3!";

        // read the config file (parse from json ...)
        Cryptonic.Config myconfig = new Cryptonic.Config().readConfig();

        List<string> groups = new List<string>();
        

        public Form1()
        {
            InitializeComponent();
        }



        // on load
        private void Form1_Load(object sender, EventArgs e)
        {

            if (File.Exists("config.cfg"))
            {

                // create the file or populate the list
                if (!File.Exists("liste_blanche.txt"))
                {
                    File.Create("liste_blanche.txt").Dispose();
                }
                else
                {
                    string[] allLines = File.ReadAllLines("liste_blanche.txt");
                    LBwhite.Items.AddRange(allLines);
                }



                if (!File.Exists("liste_noire.txt"))
                {
                    File.Create("liste_noire.txt").Dispose();
                }
                else
                {
                    string[] allLines = File.ReadAllLines("liste_noire.txt");
                    LBblack.Items.AddRange(allLines);
                }


                // add all groups to global var
                groups = getGroups();

                // populate group list
                foreach (string gp in groups)
                {

                    //bool addElement = true;

                    if (LBwhite.Items.IndexOf(gp) < 0 && LBblack.Items.IndexOf(gp) < 0)
                    {
                        LBgroups.Items.Add(gp); // add to list
                    }


                }

                // update counters
                updateCounts();

            }
            else
            {
                MessageBox.Show("Le fichier config.cfg généré par Cryptonic est manquant !", "Fichier manquant", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Form1.ActiveForm.Close();
            }


            

        }



        // get groups list
        private List<string> getGroups () {

            List<string> groupList = new List<string>();

            try
            {
                PrincipalContext ctx = new PrincipalContext(ContextType.Domain, myconfig.serverName, LDAP.EncDec.Decrypt(myconfig.serverPath, PasswordHash), LDAP.EncDec.Decrypt(myconfig.serverLogin, PasswordHash), LDAP.EncDec.Decrypt(myconfig.serverPassword, PasswordHash));

             
                // define a "query-by-example" principal - here, we search for a GroupPrincipal 
                GroupPrincipal qbeGroup = new GroupPrincipal(ctx);

                // create your principal searcher passing in the QBE principal    
                PrincipalSearcher srch = new PrincipalSearcher();
                srch.QueryFilter = qbeGroup;

                PrincipalSearchResult<Principal> result = srch.FindAll();

                // find all matches
                foreach (Principal group in result)
                {
                    groupList.Add(group.Name);

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return groupList;

        }



        // update all counters (yeah, I don't care...)
        private void updateCounts()
        {
            LAselection.Text = "Selection : " + LBgroups.SelectedIndices.Count + " / " + LBgroups.Items.Count;
            LAblack.Text = "Liste noire : " + LBblack.SelectedIndices.Count + " / " + LBblack.Items.Count;
            LAwhite.Text = "Liste blanche : " + LBwhite.SelectedIndices.Count + " / " + LBwhite.Items.Count;
        }

        // toggle one line
        private void LBgroups_SelectedIndexChanged(object sender, EventArgs e)
        {
            // update counters
            updateCounts();

        }



        // groups -> WL
        private void BTaddToWL_Click(object sender, EventArgs e)
        {


            foreach ( int itm in LBgroups.SelectedIndices )
            {
                // add in WL
                LBwhite.Items.Add( LBgroups.Items[itm]);
            }


            // second loop to delete items (never in first one !)
            foreach (string s in LBgroups.SelectedItems.OfType<string>().ToList())
            {
                LBgroups.Items.Remove(s);
            }

            // update counters
            updateCounts();


        }


        // WL -> groups
        private void BTremoveFromWL_Click(object sender, EventArgs e)
        {

            foreach (int itm in LBwhite.SelectedIndices)
            {
                // add in groups
                LBgroups.Items.Add(LBwhite.Items[itm]);
            }


            // second loop to delete items (never in first one !)
            foreach (string s in LBwhite.SelectedItems.OfType<string>().ToList())
            {
                LBwhite.Items.Remove(s);
            }


            // update counters
            updateCounts();

        }


        // groups -> BL
        private void BTaddToBL_Click(object sender, EventArgs e)
        {

            foreach (int itm in LBgroups.SelectedIndices)
            {
                // add in WL
                LBblack.Items.Add(LBgroups.Items[itm]);
            }


            // second loop to delete items (never in first one !)
            foreach (string s in LBgroups.SelectedItems.OfType<string>().ToList())
            {
                LBgroups.Items.Remove(s);
            }

            // update counters
            updateCounts();

        }

        // BL -> Groups
        private void BTremoveFromBL_Click(object sender, EventArgs e)
        {

            foreach (int itm in LBblack.SelectedIndices)
            {
                // add in groups
                LBgroups.Items.Add(LBblack.Items[itm]);
            }


            // second loop to delete items (never in first one !)
            foreach (string s in LBblack.SelectedItems.OfType<string>().ToList())
            {
                LBblack.Items.Remove(s);
            }


            // update counters
            updateCounts();

        }




        // WL selection
        private void LBwhite_SelectedIndexChanged(object sender, EventArgs e)
        {
            // update counters
            updateCounts();
        }




        // BL selection
        private void LBblack_SelectedIndexChanged(object sender, EventArgs e)
        {
            // update counters
            updateCounts();

        }



        // generate WL
        private void BTgenWL_Click(object sender, EventArgs e)
        {



            TextWriter tw = new StreamWriter("liste_blanche.txt");

            foreach (string s in LBwhite.Items)
            {
                tw.WriteLine(s);
            }

            tw.Close();
        }


        // generate BL
        private void BTgenBL_Click(object sender, EventArgs e)
        {
            TextWriter tw = new StreamWriter("liste_noire.txt");

            foreach (string s in LBblack.Items)
            {
                tw.WriteLine(s);
            }

            tw.Close();
        }



        // Filter
        private void Tfilter_TextChanged(object sender, EventArgs e)
        {


            LBgroups.Items.Clear();
            foreach (string gp in groups)
            {
                if (!string.IsNullOrEmpty(Tfilter.Text))
                {
                    if (gp.Contains(Tfilter.Text) && !LBwhite.Items.Contains(gp) && !LBblack.Items.Contains(gp))
                        LBgroups.Items.Add(gp);
                }
                else {
                    if (!LBwhite.Items.Contains(gp) && !LBblack.Items.Contains(gp))
                        LBgroups.Items.Add(gp);
                }
                
            }

        }
    }
}
