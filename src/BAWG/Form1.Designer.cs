﻿namespace BAWG
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.LBwhite = new System.Windows.Forms.ListBox();
            this.LBblack = new System.Windows.Forms.ListBox();
            this.LAselection = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.BTaddToWL = new System.Windows.Forms.Button();
            this.BTremoveFromWL = new System.Windows.Forms.Button();
            this.BTremoveFromBL = new System.Windows.Forms.Button();
            this.BTaddToBL = new System.Windows.Forms.Button();
            this.BTgenWL = new System.Windows.Forms.Button();
            this.BTgenBL = new System.Windows.Forms.Button();
            this.LBgroups = new System.Windows.Forms.ListBox();
            this.LAwhite = new System.Windows.Forms.Label();
            this.LAblack = new System.Windows.Forms.Label();
            this.Tfilter = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // LBwhite
            // 
            this.LBwhite.FormattingEnabled = true;
            this.LBwhite.Location = new System.Drawing.Point(345, 23);
            this.LBwhite.Name = "LBwhite";
            this.LBwhite.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.LBwhite.Size = new System.Drawing.Size(120, 147);
            this.LBwhite.Sorted = true;
            this.LBwhite.TabIndex = 1;
            this.LBwhite.SelectedIndexChanged += new System.EventHandler(this.LBwhite_SelectedIndexChanged);
            // 
            // LBblack
            // 
            this.LBblack.FormattingEnabled = true;
            this.LBblack.Location = new System.Drawing.Point(345, 202);
            this.LBblack.Name = "LBblack";
            this.LBblack.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.LBblack.Size = new System.Drawing.Size(120, 147);
            this.LBblack.Sorted = true;
            this.LBblack.TabIndex = 2;
            this.LBblack.SelectedIndexChanged += new System.EventHandler(this.LBblack_SelectedIndexChanged);
            // 
            // LAselection
            // 
            this.LAselection.AutoSize = true;
            this.LAselection.Location = new System.Drawing.Point(12, 339);
            this.LAselection.Name = "LAselection";
            this.LAselection.Size = new System.Drawing.Size(51, 13);
            this.LAselection.TabIndex = 6;
            this.LAselection.Text = "Selection";
            // 
            // BTaddToWL
            // 
            this.BTaddToWL.Image = global::BAWG.Properties.Resources.ic_arrow_forward_black_24dp;
            this.BTaddToWL.Location = new System.Drawing.Point(253, 59);
            this.BTaddToWL.Name = "BTaddToWL";
            this.BTaddToWL.Size = new System.Drawing.Size(75, 41);
            this.BTaddToWL.TabIndex = 9;
            this.toolTip1.SetToolTip(this.BTaddToWL, "Envoyer vers liste blanche");
            this.BTaddToWL.UseVisualStyleBackColor = true;
            this.BTaddToWL.Click += new System.EventHandler(this.BTaddToWL_Click);
            // 
            // BTremoveFromWL
            // 
            this.BTremoveFromWL.Image = global::BAWG.Properties.Resources.ic_arrow_back_black_24dp;
            this.BTremoveFromWL.Location = new System.Drawing.Point(253, 106);
            this.BTremoveFromWL.Name = "BTremoveFromWL";
            this.BTremoveFromWL.Size = new System.Drawing.Size(75, 41);
            this.BTremoveFromWL.TabIndex = 11;
            this.toolTip1.SetToolTip(this.BTremoveFromWL, "Retirer de la liste blanche");
            this.BTremoveFromWL.UseVisualStyleBackColor = true;
            this.BTremoveFromWL.Click += new System.EventHandler(this.BTremoveFromWL_Click);
            // 
            // BTremoveFromBL
            // 
            this.BTremoveFromBL.Image = global::BAWG.Properties.Resources.ic_arrow_back_black_24dp;
            this.BTremoveFromBL.Location = new System.Drawing.Point(253, 274);
            this.BTremoveFromBL.Name = "BTremoveFromBL";
            this.BTremoveFromBL.Size = new System.Drawing.Size(75, 41);
            this.BTremoveFromBL.TabIndex = 13;
            this.toolTip1.SetToolTip(this.BTremoveFromBL, "Retirer de la liste noire");
            this.BTremoveFromBL.UseVisualStyleBackColor = true;
            this.BTremoveFromBL.Click += new System.EventHandler(this.BTremoveFromBL_Click);
            // 
            // BTaddToBL
            // 
            this.BTaddToBL.Image = global::BAWG.Properties.Resources.ic_arrow_forward_black_24dp;
            this.BTaddToBL.Location = new System.Drawing.Point(253, 227);
            this.BTaddToBL.Name = "BTaddToBL";
            this.BTaddToBL.Size = new System.Drawing.Size(75, 41);
            this.BTaddToBL.TabIndex = 12;
            this.toolTip1.SetToolTip(this.BTaddToBL, "Envoyer vers liste noire");
            this.BTaddToBL.UseVisualStyleBackColor = true;
            this.BTaddToBL.Click += new System.EventHandler(this.BTaddToBL_Click);
            // 
            // BTgenWL
            // 
            this.BTgenWL.Image = global::BAWG.Properties.Resources.ic_folder_open_black_48dp;
            this.BTgenWL.Location = new System.Drawing.Point(491, 59);
            this.BTgenWL.Name = "BTgenWL";
            this.BTgenWL.Size = new System.Drawing.Size(75, 53);
            this.BTgenWL.TabIndex = 16;
            this.toolTip1.SetToolTip(this.BTgenWL, "Générer le fichier de liste blanche");
            this.BTgenWL.UseVisualStyleBackColor = true;
            this.BTgenWL.Click += new System.EventHandler(this.BTgenWL_Click);
            // 
            // BTgenBL
            // 
            this.BTgenBL.Image = global::BAWG.Properties.Resources.ic_folder_black_48dp;
            this.BTgenBL.Location = new System.Drawing.Point(491, 247);
            this.BTgenBL.Name = "BTgenBL";
            this.BTgenBL.Size = new System.Drawing.Size(75, 53);
            this.BTgenBL.TabIndex = 17;
            this.toolTip1.SetToolTip(this.BTgenBL, "Générer le fichier de liste noire");
            this.BTgenBL.UseVisualStyleBackColor = true;
            this.BTgenBL.Click += new System.EventHandler(this.BTgenBL_Click);
            // 
            // LBgroups
            // 
            this.LBgroups.FormattingEnabled = true;
            this.LBgroups.Location = new System.Drawing.Point(12, 38);
            this.LBgroups.Name = "LBgroups";
            this.LBgroups.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.LBgroups.Size = new System.Drawing.Size(228, 290);
            this.LBgroups.Sorted = true;
            this.LBgroups.TabIndex = 10;
            this.LBgroups.SelectedIndexChanged += new System.EventHandler(this.LBgroups_SelectedIndexChanged);
            // 
            // LAwhite
            // 
            this.LAwhite.AutoSize = true;
            this.LAwhite.Location = new System.Drawing.Point(342, 7);
            this.LAwhite.Name = "LAwhite";
            this.LAwhite.Size = new System.Drawing.Size(102, 13);
            this.LAwhite.TabIndex = 14;
            this.LAwhite.Text = "Liste blanche : 0 / 0";
            // 
            // LAblack
            // 
            this.LAblack.AutoSize = true;
            this.LAblack.Location = new System.Drawing.Point(342, 186);
            this.LAblack.Name = "LAblack";
            this.LAblack.Size = new System.Drawing.Size(87, 13);
            this.LAblack.TabIndex = 15;
            this.LAblack.Text = "Liste noire : 0 / 0";
            // 
            // Tfilter
            // 
            this.Tfilter.Location = new System.Drawing.Point(56, 12);
            this.Tfilter.Name = "Tfilter";
            this.Tfilter.Size = new System.Drawing.Size(184, 20);
            this.Tfilter.TabIndex = 18;
            this.Tfilter.TextChanged += new System.EventHandler(this.Tfilter_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Filtre : ";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(586, 380);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Tfilter);
            this.Controls.Add(this.BTgenBL);
            this.Controls.Add(this.BTgenWL);
            this.Controls.Add(this.LAblack);
            this.Controls.Add(this.LAwhite);
            this.Controls.Add(this.BTremoveFromBL);
            this.Controls.Add(this.BTaddToBL);
            this.Controls.Add(this.BTremoveFromWL);
            this.Controls.Add(this.LBgroups);
            this.Controls.Add(this.BTaddToWL);
            this.Controls.Add(this.LAselection);
            this.Controls.Add(this.LBblack);
            this.Controls.Add(this.LBwhite);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "BAWG v20190122";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListBox LBwhite;
        private System.Windows.Forms.ListBox LBblack;
        private System.Windows.Forms.Label LAselection;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button BTaddToWL;
        private System.Windows.Forms.ListBox LBgroups;
        private System.Windows.Forms.Button BTremoveFromWL;
        private System.Windows.Forms.Button BTremoveFromBL;
        private System.Windows.Forms.Button BTaddToBL;
        private System.Windows.Forms.Label LAwhite;
        private System.Windows.Forms.Label LAblack;
        private System.Windows.Forms.Button BTgenWL;
        private System.Windows.Forms.Button BTgenBL;
        private System.Windows.Forms.TextBox Tfilter;
        private System.Windows.Forms.Label label1;
    }
}

