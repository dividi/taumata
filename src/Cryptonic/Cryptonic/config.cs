﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Newtonsoft.Json;


namespace Cryptonic
{


    /**
     * 
     * Class to simplify read/write the json config file
     * 
     * */

    public class Config
    {
        public string serverName;
        public string serverLogin;
        public string serverPassword;
        public string serverAddress;
        public string serverPath;
        public string cacheDelay;
        public string groupFilter;
        public string showEmptyGroup;
        public string debugMode;
        public string sizeLimit;
        public string showDisabled;
        public string distriFolder;


        // write object to file 
        public void writeConfig()
        {

            StreamWriter sw = new StreamWriter("config.cfg", false, System.Text.Encoding.UTF8);

            // serialize all the groups        
            sw.WriteLine(JsonConvert.SerializeObject(this));

            // close the streamwriter
            sw.Close();

        }





        // read the config file and spit an object
        public Config readConfig()
        {
            // deszrialize the JSON file
            StreamReader sr = new StreamReader("config.cfg", Encoding.UTF8);

            Config myconfig = new Config();

            try
            {
                myconfig = JsonConvert.DeserializeObject<Config>(sr.ReadToEnd());
                
            }
            catch (Exception ex)
            {
                Console.WriteLine("JSON parse error " + ex.Message);
            }
            
            sr.Close();

            return myconfig;

        }

    }
}
