﻿namespace Cryptonic
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.BT_crypt = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.Tlogin = new System.Windows.Forms.TextBox();
            this.Tpass = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Tldap = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TldapPath = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.Thide = new System.Windows.Forms.TextBox();
            this.TldapName = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.enableDebug = new System.Windows.Forms.CheckBox();
            this.TLimit = new System.Windows.Forms.NumericUpDown();
            this.Tcache = new System.Windows.Forms.NumericUpDown();
            this.showEmptyGroups = new System.Windows.Forms.CheckBox();
            this.showDisabled = new System.Windows.Forms.CheckBox();
            this.BTwhiteList = new System.Windows.Forms.Button();
            this.BTblackList = new System.Windows.Forms.Button();
            this.TdistriFolder = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.deleteJson = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.TLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tcache)).BeginInit();
            this.SuspendLayout();
            // 
            // BT_crypt
            // 
            this.BT_crypt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.BT_crypt.Location = new System.Drawing.Point(253, 170);
            this.BT_crypt.Name = "BT_crypt";
            this.BT_crypt.Size = new System.Drawing.Size(204, 46);
            this.BT_crypt.TabIndex = 0;
            this.BT_crypt.Text = "Crypt";
            this.BT_crypt.UseVisualStyleBackColor = false;
            this.BT_crypt.Click += new System.EventHandler(this.BT_crypt_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Utilisateur *";
            // 
            // Tlogin
            // 
            this.Tlogin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Tlogin.Location = new System.Drawing.Point(110, 33);
            this.Tlogin.Name = "Tlogin";
            this.Tlogin.Size = new System.Drawing.Size(100, 20);
            this.Tlogin.TabIndex = 1;
            this.toolTip1.SetToolTip(this.Tlogin, "Votre nom d\'utilisateur ADM (ex: BATCAVE\\bwayneadm)");
            this.Tlogin.WordWrap = false;
            // 
            // Tpass
            // 
            this.Tpass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Tpass.Location = new System.Drawing.Point(110, 59);
            this.Tpass.Name = "Tpass";
            this.Tpass.PasswordChar = '*';
            this.Tpass.Size = new System.Drawing.Size(100, 20);
            this.Tpass.TabIndex = 2;
            this.toolTip1.SetToolTip(this.Tpass, "Votre mot de passe du compte (ex:**********)");
            this.Tpass.WordWrap = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Password *";
            // 
            // Tldap
            // 
            this.Tldap.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Tldap.Location = new System.Drawing.Point(110, 85);
            this.Tldap.Name = "Tldap";
            this.Tldap.Size = new System.Drawing.Size(100, 20);
            this.Tldap.TabIndex = 3;
            this.toolTip1.SetToolTip(this.Tldap, "Adresse du serveur LDAP (ex: 192.168.0.23)");
            this.Tldap.WordWrap = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Serveur LDAP *";
            // 
            // TldapPath
            // 
            this.TldapPath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TldapPath.Location = new System.Drawing.Point(110, 111);
            this.TldapPath.Name = "TldapPath";
            this.TldapPath.Size = new System.Drawing.Size(100, 20);
            this.TldapPath.TabIndex = 4;
            this.toolTip1.SetToolTip(this.TldapPath, "chemin LDAP vers les utilisateurs (ex: OU=0132205B,OU=MrsCentre,OU=Utilisateurs P" +
        "rovisoires,DC=dividi,DC=org )");
            this.TldapPath.WordWrap = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 111);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Chemin LDAP *";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(250, 7);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Jours Cache *";
            // 
            // toolTip1
            // 
            this.toolTip1.IsBalloon = true;
            this.toolTip1.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            // 
            // Thide
            // 
            this.Thide.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Thide.Location = new System.Drawing.Point(357, 59);
            this.Thide.Name = "Thide";
            this.Thide.Size = new System.Drawing.Size(100, 20);
            this.Thide.TabIndex = 7;
            this.toolTip1.SetToolTip(this.Thide, "Permet de masquer une partie du nom du groupe (ex: 0132205B-)");
            this.Thide.WordWrap = false;
            // 
            // TldapName
            // 
            this.TldapName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TldapName.Location = new System.Drawing.Point(110, 7);
            this.TldapName.Name = "TldapName";
            this.TldapName.Size = new System.Drawing.Size(100, 20);
            this.TldapName.TabIndex = 1;
            this.toolTip1.SetToolTip(this.TldapName, "Le nom du serveur LDAP (sercol.lan, dividi.org, batcave.com, ...)");
            this.TldapName.WordWrap = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 7);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Nom LDAP *";
            this.toolTip1.SetToolTip(this.label9, "Le nom du serveur LDAP (par ex: sercol.lan)");
            // 
            // enableDebug
            // 
            this.enableDebug.AutoSize = true;
            this.enableDebug.Location = new System.Drawing.Point(357, 34);
            this.enableDebug.Name = "enableDebug";
            this.enableDebug.Size = new System.Drawing.Size(15, 14);
            this.enableDebug.TabIndex = 21;
            this.toolTip1.SetToolTip(this.enableDebug, "Le mode debug permet d\'enrichir les logs avec des données plus précises");
            this.enableDebug.UseVisualStyleBackColor = true;
            // 
            // TLimit
            // 
            this.TLimit.Location = new System.Drawing.Point(357, 134);
            this.TLimit.Maximum = new decimal(new int[] {
            1024,
            0,
            0,
            0});
            this.TLimit.Name = "TLimit";
            this.TLimit.Size = new System.Drawing.Size(59, 20);
            this.TLimit.TabIndex = 25;
            this.toolTip1.SetToolTip(this.TLimit, "Limite la taille de l\'envoi des fichiers en Mo (0 pour pas de limite)");
            // 
            // Tcache
            // 
            this.Tcache.Location = new System.Drawing.Point(357, 5);
            this.Tcache.Maximum = new decimal(new int[] {
            365,
            0,
            0,
            0});
            this.Tcache.Name = "Tcache";
            this.Tcache.Size = new System.Drawing.Size(59, 20);
            this.Tcache.TabIndex = 26;
            this.toolTip1.SetToolTip(this.Tcache, "Durée du cache en jours avant réécriture de l\'arbre LDAP");
            // 
            // showEmptyGroups
            // 
            this.showEmptyGroups.AutoSize = true;
            this.showEmptyGroups.Location = new System.Drawing.Point(357, 113);
            this.showEmptyGroups.Name = "showEmptyGroups";
            this.showEmptyGroups.Size = new System.Drawing.Size(15, 14);
            this.showEmptyGroups.TabIndex = 19;
            this.toolTip1.SetToolTip(this.showEmptyGroups, "Affiche les groupes vides");
            this.showEmptyGroups.UseVisualStyleBackColor = true;
            // 
            // showDisabled
            // 
            this.showDisabled.AutoSize = true;
            this.showDisabled.Location = new System.Drawing.Point(357, 88);
            this.showDisabled.Name = "showDisabled";
            this.showDisabled.Size = new System.Drawing.Size(15, 14);
            this.showDisabled.TabIndex = 28;
            this.toolTip1.SetToolTip(this.showDisabled, "Afficher les utilisateurs désactivés");
            this.showDisabled.UseVisualStyleBackColor = true;
            // 
            // BTwhiteList
            // 
            this.BTwhiteList.BackColor = System.Drawing.Color.White;
            this.BTwhiteList.Location = new System.Drawing.Point(10, 170);
            this.BTwhiteList.Name = "BTwhiteList";
            this.BTwhiteList.Size = new System.Drawing.Size(86, 23);
            this.BTwhiteList.TabIndex = 32;
            this.BTwhiteList.Text = "Liste Blanche";
            this.toolTip1.SetToolTip(this.BTwhiteList, "Ouvre le fichier de la liste Blanche (attention : sensible à la casse)");
            this.BTwhiteList.UseVisualStyleBackColor = false;
            this.BTwhiteList.Click += new System.EventHandler(this.BTwhiteList_Click);
            // 
            // BTblackList
            // 
            this.BTblackList.BackColor = System.Drawing.Color.Black;
            this.BTblackList.ForeColor = System.Drawing.Color.White;
            this.BTblackList.Location = new System.Drawing.Point(10, 193);
            this.BTblackList.Name = "BTblackList";
            this.BTblackList.Size = new System.Drawing.Size(86, 23);
            this.BTblackList.TabIndex = 33;
            this.BTblackList.Text = "Liste Noire";
            this.toolTip1.SetToolTip(this.BTblackList, "Ouvre le fichier de la liste noire (attention : sensible à la casse)");
            this.BTblackList.UseVisualStyleBackColor = false;
            this.BTblackList.Click += new System.EventHandler(this.BTblackList_Click);
            // 
            // TdistriFolder
            // 
            this.TdistriFolder.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TdistriFolder.Location = new System.Drawing.Point(110, 134);
            this.TdistriFolder.Name = "TdistriFolder";
            this.TdistriFolder.Size = new System.Drawing.Size(100, 20);
            this.TdistriFolder.TabIndex = 34;
            this.toolTip1.SetToolTip(this.TdistriFolder, "Nom du dossier où sont distribués les fichiers (Taumata si vide)");
            this.TdistriFolder.WordWrap = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(250, 61);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Masquer préfixe";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(250, 113);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(75, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "Groupes vides";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(250, 34);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 13);
            this.label11.TabIndex = 20;
            this.label11.Text = "Debug mode";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(250, 136);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 13);
            this.label5.TabIndex = 23;
            this.label5.Text = "Limite";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(416, 141);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(22, 13);
            this.label12.TabIndex = 24;
            this.label12.Text = "Mo";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(250, 87);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(88, 13);
            this.label13.TabIndex = 27;
            this.label13.Text = "Users désactivés";
            // 
            // deleteJson
            // 
            this.deleteJson.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.deleteJson.ForeColor = System.Drawing.SystemColors.ControlText;
            this.deleteJson.Location = new System.Drawing.Point(126, 170);
            this.deleteJson.Name = "deleteJson";
            this.deleteJson.Size = new System.Drawing.Size(99, 46);
            this.deleteJson.TabIndex = 31;
            this.deleteJson.Text = "Vider le cache LDAP";
            this.deleteJson.UseVisualStyleBackColor = false;
            this.deleteJson.Click += new System.EventHandler(this.deleteJson_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 136);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 13);
            this.label7.TabIndex = 35;
            this.label7.Text = "Dossier de Distri";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 232);
            this.Controls.Add(this.TdistriFolder);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.BTblackList);
            this.Controls.Add(this.BTwhiteList);
            this.Controls.Add(this.deleteJson);
            this.Controls.Add(this.showDisabled);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.Tcache);
            this.Controls.Add(this.TLimit);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.enableDebug);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.showEmptyGroups);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.TldapName);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.Thide);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TldapPath);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Tldap);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Tpass);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Tlogin);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BT_crypt);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Cryptonic pour Taumata... v20190122";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.TLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tcache)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BT_crypt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Tlogin;
        private System.Windows.Forms.TextBox Tpass;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Tldap;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TldapPath;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TextBox Thide;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox TldapName;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox showEmptyGroups;
        private System.Windows.Forms.CheckBox enableDebug;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown TLimit;
        private System.Windows.Forms.NumericUpDown Tcache;
        private System.Windows.Forms.CheckBox showDisabled;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button deleteJson;
        private System.Windows.Forms.Button BTwhiteList;
        private System.Windows.Forms.Button BTblackList;
        private System.Windows.Forms.TextBox TdistriFolder;
        private System.Windows.Forms.Label label7;
    }
}

