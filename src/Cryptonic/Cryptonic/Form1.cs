﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;


namespace Cryptonic
{
    public partial class Form1 : Form
    {
        // The key to decrypt/encrypt the file
        static readonly string PasswordHash = "Al3x1s,C'e$tM0nPet!tP0t3!";

        public Form1()
        {
            InitializeComponent();
        }

        private void BT_crypt_Click(object sender, EventArgs e)
        {
            if (Tlogin.Text != "" && Tpass.Text != "" && Tldap.Text != "" && TldapPath.Text != "" && TldapName.Text != "" && Tcache.Text != "")
            {
                string login = EncDec.Encrypt(Tlogin.Text, PasswordHash);
                string pass = EncDec.Encrypt(Tpass.Text, PasswordHash);
                string ldap = EncDec.Encrypt(Tldap.Text, PasswordHash); // need it anymore ???
                string ldapPath = EncDec.Encrypt(TldapPath.Text, PasswordHash);
                string sizeLimit = TLimit.Text;
                string distriFolder = TdistriFolder.Text.Trim();
                string emptyGroups = "0";
                string debug = "0";
                string disabled = "0";

                // default case
                if (distriFolder == "") {
                    distriFolder = "Taumata";
                    TdistriFolder.Text = "Taumata";
                }


                if (showEmptyGroups.Checked)
                {
                    emptyGroups = "1";
                }
                               

                if (enableDebug.Checked)
                {
                    debug = "1";
                }


                if (showDisabled.Checked)
                {
                    disabled = "1";
                }


                Config config = new Config();

                // config properties
                config.serverLogin = login;
                config.serverPassword = pass;
                config.serverAddress =ldap ;    // need it anymore ???
                config.serverPath = ldapPath;
                config.cacheDelay =Tcache.Text ;
                config.groupFilter = Thide.Text;
                config.showEmptyGroup = emptyGroups;
                config.serverName = TldapName.Text;
                config.debugMode = debug;
                config.showDisabled = disabled;
                config.sizeLimit = TLimit.Text;
                config.distriFolder = distriFolder;


                // writing the object to the file
                config.writeConfig();

                MessageBox.Show("Le fichier config.ini a été créé.", "Information", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);

            }
            else {
                MessageBox.Show("Tous les champs * doivent être remplis !", "Erreur", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            }
        
        } // end of crypt-click






        private void Form1_Load(object sender, EventArgs e)
        {

            // fill the field if file exists
            if ( File.Exists("config.cfg") ) {

                
                // Read the config file
                Config config = new Config().readConfig();


                try {
                    Tlogin.Text = EncDec.Decrypt(config.serverLogin, PasswordHash);
                    Tldap.Text = EncDec.Decrypt(config.serverAddress, PasswordHash);
                    TldapPath.Text = EncDec.Decrypt(config.serverPath, PasswordHash);
                    Tcache.Text = config.cacheDelay;
                    Thide.Text = config.groupFilter;
                    TldapName.Text = config.serverName;
                    TLimit.Text = config.sizeLimit;
                    TdistriFolder.Text = config.distriFolder;

                    if (config.showEmptyGroup == "1") {
                        showEmptyGroups.Checked = true;
                    }

                    if (config.debugMode == "1")
                    {
                        enableDebug.Checked = true;
                    }


                    if (config.showDisabled == "1")
                    {
                        showDisabled.Checked = true;
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
               
            }

        }


        // delete the LDAP.json cache file
        private void deleteJson_Click(object sender, EventArgs e)
        {
            try
            {
                File.Delete("LDAP.json");

            }
            catch(Exception ex)
            {
                MessageBox.Show("Erreur lors de la suppression du fichier de cache : " + ex.Message);
            }

        }


        // open whitelist
        private void BTwhiteList_Click(object sender, EventArgs e)
        {
            if (!File.Exists(@"liste_blanche.txt")) {
                File.Create(@"liste_blanche.txt").Dispose();
            }
            
            Process.Start(@"liste_blanche.txt");


        }

        // open blacklist
        private void BTblackList_Click(object sender, EventArgs e)
        {
            if (!File.Exists(@"liste_noire.txt")) {
                File.Create(@"liste_noire.txt").Dispose();
            }

            Process.Start(@"liste_noire.txt");
        }
    } // end of class
}

