﻿/*
 * TAUMATA
 * 
 *  MAIN SECTION + Customize CLASS
 *  DAVID LE VIAVANT 2016
 *  LICENCE GPL3
 * 
 * */


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using System.IO;
using Newtonsoft.Json;

using System.Threading;
using System.Threading.Tasks;
using System.Security.AccessControl;

  
namespace LDAP
{


    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }


        // read the config file (parse from json ...)
        static Cryptonic.Config myconfig = new Cryptonic.Config().readConfig();

        // get the white and black lists
        static List<string> whitelist = new List<string>(File.ReadAllLines("liste_blanche.txt", Encoding.UTF8));
        static List<string> blacklist = new List<string>(File.ReadAllLines("liste_noire.txt", Encoding.UTF8));  // or File.ReadAllLines(path, Encoding.UTF8).ToList();

        // passphrase to decode config file
        string PasswordHash = "Al3x1s,C'e$tM0nPet!tP0t3!";

        // send to list
        Dictionary<string, string> sendToList = new Dictionary<string, string>();

        // User parameters (+ groups)
        Customize customParams = new Customize();



        #region GUI




        ////////////////////////////////////////////////////////////////////
        //          at the beginning, there was the Word
        ////////////////////////////////////////////////////////////////////
        private void Form1_Load(object sender, EventArgs e)
        {

            // If config file exists OK, else -> see ya space cowboy
            if (File.Exists("config.cfg"))
            {
                log("[BOOT] Hello", false);

    
                // Init LDAP connexion
                LDAP myldap = new LDAP(myconfig, PasswordHash, whitelist, blacklist);


                // If LDAP.json file exists and not out of date
                if (File.Exists("LDAP.json") && DateTime.Today.CompareTo(File.GetCreationTime("LDAP.json").AddDays(Convert.ToInt16(myconfig.cacheDelay))) == -1)
                {

                    log("[BOOT] Cache file OK", true);

                    // import in treeview
                    //myldap.importJSON(TV_groups, "LDAP.json", true);
                    myldap.importJSON(TV_groups, "LDAP.json", Convert.ToBoolean(Convert.ToInt32(myconfig.showEmptyGroup)));

                    // Sort the treeview
                    TV_groups.Sort();



                    // Load customize file
                    if (File.Exists(Environment.UserName + ".json"))
                    {
                        customParams = customParams.loadCustomizeFile(Environment.UserName);

                        // Fill the list
                        loadGroupsList(customParams);

                    }
                    
                }



                // Else, populate treeview from AD and create the file
                else
                {
                    log("[BOOT] Cache file NOK, creating it", true);


                   Thread mythread = new Thread(() => new waitForm().ShowDialog());
                    mythread.Start();


                    // Delete the LDAP file (else the creation date will not be changed !)
                    if (File.Exists("LDAP.json")) {
                        File.Delete("LDAP.json");
                    }


                    // create the cache file
                    myldap.exportToJSON("LDAP.json");

                    // import in treeview (Convert to boolean... change 0 to false and so)
                    myldap.importJSON(TV_groups, "LDAP.json", Convert.ToBoolean(Convert.ToInt32(myconfig.showEmptyGroup)));

                    // Sort the treeview
                    TV_groups.Sort();

                    mythread.Abort();
                }

            }
            else
            {
                MessageBox.Show("Le fichier config.cfg n'existe pas !", "GURU MEDITATION", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                log("[BOOT] GURU MEDITATION : no config.cfg file !", false);
            }

        }




        ////////////////////////////////////////////////////////////////////
        //          Choose the folder to deliver
        ////////////////////////////////////////////////////////////////////
        private void openFolderBrowser_Click(object sender, EventArgs e)
        {

            // show the folder dialog
            DialogResult result = sendFolderBrowser.ShowDialog();


            // OK was pressed
            if (result == DialogResult.OK)
            {
                if (!addFolderToFileview(sendFolderBrowser.SelectedPath))
                {
                    MessageBox.Show("Le volume des fichiers est trop important (Limite fixée à " + Int32.Parse(myconfig.sizeLimit) * 1024 + "Ko).", "Fichiers trop lourds", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

            }


        }


        ////////////////////////////////////////////////////////////////////
        //          Choose the files to deliver
        ////////////////////////////////////////////////////////////////////
        private void openFileBrowser_Click(object sender, EventArgs e)
        {
            fileBrowser.ShowDialog();
        }


        ////////////////////////////////////////////////////////////////////
        //          After choosing the files to deliver
        ////////////////////////////////////////////////////////////////////
        private void fileBrowser_FileOk(object sender, CancelEventArgs e)
        {

            foreach(string file in fileBrowser.FileNames)
            {
                if (!addFileToFileview(file))
                {
                    MessageBox.Show("Le volume des fichiers est trop important (Limite fixée à " + Int32.Parse(myconfig.sizeLimit) * 1024 + "Ko).", "Fichiers trop lourds", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
        }





        // Show the user folder
        private void BT_showFolder_Click(object sender, EventArgs e)
        {
            if (sendToList.Count == 1)
            {

                try
                {
                    string loggedUserDN = UserPrincipal.Current.DisplayName;

                    log("Try to open folder : " + sendToList.First().Key + "\\" + myconfig.distriFolder + "\\" + loggedUserDN, false);
                    System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo()
                    {
                        FileName = sendToList.First().Key + "\\" + myconfig.distriFolder + "\\" + loggedUserDN,
                        UseShellExecute = true,
                        Verb = "open"
                    });
                }
                catch(Exception ex)
                {
                    MessageBox.Show("" + ex.Message, "Impossible de visionner le dossier utilisateur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    log("[WARN] Open folder :" + ex.Message, false);
                }
            }
            else
            {
                MessageBox.Show("Merci de ne sélectionner qu'un seul utilisateur");
            }
        }






        ////////////////////////////////////////////////////////////////////
        //          Add one file to the list
        ////////////////////////////////////////////////////////////////////
        private Boolean addFileToFileview(string file)
        {

            if (testFileSizeCheck(file))
            {
                fileView.Items.Add("[FICHIER] " + Path.GetFileName(file), file);
                return true;
            }
            else {
                return false;
            }
        }



        // add a folder to the list
        private Boolean addFolderToFileview(string folder)
        {

            if (testFolderSizeCheck(folder))
            {
                fileView.Items.Add("[DOSSIER] " + new DirectoryInfo(folder).Name, folder);
                return true;
            }
            else
            {
                return false;
            }


        }




        // test if a file pass the size check
        private Boolean testFileSizeCheck(string file)
        {
            // initial files size
            long filesSize = getFileviewSize(fileView);


            // limit size
            int maxsize = 0;

            // init if null or zero
            if (myconfig.sizeLimit == "")
            {
                maxsize = 0;
            }
            else
            {
                maxsize = Int32.Parse(myconfig.sizeLimit);
            }


            // add file size to general weight
            filesSize += (int)new FileInfo(file).Length;


            // if too big or disabled (=0)
            if ((filesSize / 1024) > maxsize * 1024 && maxsize != 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }





        // test if a folder pass the size check
        private Boolean testFolderSizeCheck(string folder)
        {
            // initial files size + selected folder size
            //long filesSize = getFileviewSize(fileView) + getFolderSize(folder);
            long filesSize = getFileviewSize(fileView);


            // limit size
            int maxsize = 0;

            // init if null or zero
            if (myconfig.sizeLimit == "")
            {
                maxsize = 0;
            }
            else
            {
                maxsize = Int32.Parse(myconfig.sizeLimit);
            }


            log("list viewsize  : " + filesSize, true);

            log("max size : " + maxsize, true);



            // if too big or disabled (=0)
            if ((filesSize / 1024) > maxsize * 1024 && maxsize != 0)
            {
                return false;
            }
            else // add the folder
            {
                return true;
            }
        }







        // On drop a file, a folder...
        private void fileView_DragDrop(object sender, DragEventArgs e)
        {

            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);


            foreach (string file in files)
            {
                
                // get the file attributes for file or directory
                FileAttributes attr = File.GetAttributes(file);

                if (attr.HasFlag(FileAttributes.Directory))
                {
                    if (!addFolderToFileview(file))
                    {
                        MessageBox.Show("Le volume des fichiers est trop important (Limite fixée à " + Int32.Parse(myconfig.sizeLimit) * 1024 + "Ko).", "Fichiers trop lourds", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }

                else
                {
                    if (!addFileToFileview(file))
                    {
                        MessageBox.Show("Le volume des fichiers est trop important (Limite fixée à " + Int32.Parse(myconfig.sizeLimit) * 1024 + "Ko).", "Fichiers trop lourds", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }


            }
        }


        // On drag enter the fileview
        private void fileView_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) e.Effect = DragDropEffects.Copy;
        }






        ////////////////////////////////////////////////////////////////////
        // Populate the "send to" box with checked users
        ////////////////////////////////////////////////////////////////////
        private void TV_groups_AfterCheck(object sender, TreeViewEventArgs e)
        {
            // add
            if (e.Node.Checked == true)
            {

                // congrats it's a group !

                if (e.Node.Nodes.Count > 0 || e.Node.Level == 0)
                {
                    e.Node.Expand();

                    foreach (TreeNode n in e.Node.Nodes)
                    {
                        n.Checked = true;
                    }
                }
                // oups a user
                else
                {
                    try
                    {
                        sendToList.Add(e.Node.Name, e.Node.Parent.Text + "/" + e.Node.Text);
                    }
                    catch (Exception ex)
                    {
                        log("[ERROR] Error selecting or adding a user : " + ex.Message, true);
                    }

                }

            }

            // remove
            else
            {
                // a group
                if (e.Node.Nodes.Count > 0 || e.Node.Level == 0)
                {
                    foreach (TreeNode n in e.Node.Nodes)
                    {
                        n.Checked = false;
                    }

                    e.Node.Collapse();
                }
                else
                {
                    // user
                    sendToList.Remove(e.Node.Name);
                }
            }

            //e.Node.Parent

            Tselected.Text = sendToList.Count + " personne(s) sélectionnée(s)";

            // if no user selected, disabled send button
            if (sendToList.Count > 0) {
                BT_distribuer.Enabled = true;
            }
            else
            {
                BT_distribuer.Enabled = false;
            }



        }



        ////////////////////////////////////////////////////////////////////
        // Clear all files to deliver
        ////////////////////////////////////////////////////////////////////
        private void Bvider_Click(object sender, EventArgs e)
        {
            fileView.Items.Clear();
            log("[INFO] Clear files list", true);
        }



        ////////////////////////////////////////////////////////////////////
        // Clear selected files to deliver
        ////////////////////////////////////////////////////////////////////
        private void Tsuppr_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem itm in fileView.SelectedItems)
            {
                fileView.Items.Remove(itm);
                log("[INFO] Remove a file from the list", true);
            }
        }
        #endregion





        #region SEND FILES





        ////////////////////////////////////////////////////////////////////
        //                Copy files in remote folder
        ////////////////////////////////////////////////////////////////////





        // SEND
        private void bwSend_DoWork(object sender, DoWorkEventArgs e)
        {

            BackgroundWorker worker = (BackgroundWorker)sender;

            // not cool, but hey, it is workin'
            Control.CheckForIllegalCrossThreadCalls = false;

            // Tuple to pack the arguments
            Tuple<Dictionary<string, string>, string, ListView> mytuple = (Tuple<Dictionary<string, string>, string, ListView>)e.Argument;
            Dictionary<string, string> sendToList = mytuple.Item1;
            string loggedUserDN = mytuple.Item2;
            ListView files = mytuple.Item3;

            string domain = EncDec.Decrypt(myconfig.serverLogin, PasswordHash).Split('\\')[0];
            string username = EncDec.Decrypt(myconfig.serverLogin, PasswordHash).Split('\\')[1];
            string password = EncDec.Decrypt(myconfig.serverPassword, PasswordHash);
           

            // Execute job with some other creds
            using (new Impersonator(username, domain, password))
            {
                ArrayList erreurs = new ArrayList();

                // For each users to send to

                int userCount = 0;

                foreach (KeyValuePair<string, string> user in sendToList)
                {
                    userCount++;

                    string homedir = user.Key;
                    string name = user.Value;

                    log("[SEND] working on current user : " + name + " with homedir :" + homedir, true);

                    try
                    {

                        // Create the "taumata" folder
                        if (!Directory.Exists(homedir + "\\" + myconfig.distriFolder))
                        {
                            Directory.CreateDirectory(homedir + "\\" + myconfig.distriFolder);
                            log("[SEND] Try to create "+ myconfig.distriFolder + " folder", true);
                        }


                        // Create the sender folder in the taumata folder
                        if (!Directory.Exists(homedir + "\\"+ myconfig.distriFolder + "\\" + loggedUserDN))
                        {
                            Directory.CreateDirectory(homedir + "\\"+ myconfig.distriFolder + "\\" + loggedUserDN);

                            log("[SEND] Try to create the sender folder : " + loggedUserDN, true);
                        }


                        // Create the destination folder in the taumata/user folder if field is not empty
                        // Create "Nom du devoir" subfolder
                        if (!Directory.Exists(homedir + "\\"+ myconfig.distriFolder + "\\" + loggedUserDN + "\\" + TdestFolder.Text) && TdestFolder.Text != "")
                        {
                            Directory.CreateDirectory(homedir + "\\"+ myconfig.distriFolder + "\\" + loggedUserDN + "\\" + TdestFolder.Text);

                            log("[SEND] creating " + Tnomdevoir.Text + " subfolder", false);
                        }

                        try
                        {
                            // give read rights on the folder
                            string currentuser = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                            DirectoryInfo info = new DirectoryInfo(homedir + "\\" + myconfig.distriFolder + "\\" + loggedUserDN);
                            FileSystemRights rights = FileSystemRights.Modify | FileSystemRights.ReadAndExecute | FileSystemRights.ListDirectory | FileSystemRights.Read | FileSystemRights.Write;
                            InheritanceFlags inFlags = InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit;
                            DirectorySecurity security = info.GetAccessControl(AccessControlSections.Access);
                            security.AddAccessRule(new FileSystemAccessRule(currentuser, rights, inFlags, PropagationFlags.NoPropagateInherit, AccessControlType.Allow));
                            info.SetAccessControl(security);

                        } catch(Exception ex)
                        {
                            log("[ACL ] Error : " + ex.Message, true);
                        }



                        //////////////////////////////////
                        //         COPYING
                        //////////////////////////////////



                        // For each files
                        foreach (ListViewItem file in files.Items)
                        {


                            // get the real path for U drive
                            string realPathFile;

                            // send to destination folder 
                            string destination = "";



                            // if use the bind of the homedir
                            if (Path.GetPathRoot(file.ImageKey) == "U:\\")
                            {
                                realPathFile = file.ImageKey.Replace("U:\\", System.DirectoryServices.AccountManagement.UserPrincipal.Current.HomeDirectory + "\\");
                                log("[SEND] Replacing U: with " + realPathFile, true);
                            }
                            else
                            {
                                realPathFile = file.ImageKey;
                            }

                            log("[SEND] Real path " + realPathFile, true);








                            // --------------------- good news everyone, it's a FILE !
                            if (file.Text.Substring(0, 9) == "[FICHIER]") {

                                log("[SEND] Processing : " + file.Text + " with fileName : " + Path.GetFileName(file.ImageKey), true);


                                if (TdestFolder.Text != "")
                                {
                                    destination = homedir + "\\"+ myconfig.distriFolder + "\\" + loggedUserDN + "\\" + TdestFolder.Text + "\\" + Path.GetFileName(file.ImageKey);
                                }
                                else
                                {
                                    destination = homedir + "\\"+ myconfig.distriFolder + "\\" + loggedUserDN + "\\" + Path.GetFileName(file.ImageKey);
                                }


                                // The file will not be overwritten
                                if (!File.Exists(destination))
                                {
                                    log("[SEND] File does not exist !", true);

                                    try
                                    {
                                        File.Copy(realPathFile, destination);
                                        log("[SEND] Try to copy file", true);
                                    }
                                    catch (Exception Ex)
                                    {
                                        Console.WriteLine("File error : " + Ex.Message);
                                        log("[SEND] ERROR on file : " + Ex.Message, false);
                                    }
                                    finally
                                    {
                                        //progressBarUsers.Value = progressBarUsers.Value + 1; 
                                    }

                                } // if file not exists in destination

                            }

                     



                            // ----------------------- good news everyone, it's a FOLDER !
                            if (file.Text.Substring(0, 9) == "[DOSSIER]")
                            {
                                log("[SEND] Processing : " + file.Text + " with fileName : " + file.ImageKey, true);

                                string folderName = file.Text.Replace("[DOSSIER] ", "");

                                if (TdestFolder.Text != "")
                                {
                                    destination = homedir + "\\"+ myconfig.distriFolder + "\\" + loggedUserDN + "\\" + TdestFolder.Text + "\\" + folderName;
                                }
                                else
                                {
                                    destination = homedir + "\\"+ myconfig.distriFolder + "\\" + loggedUserDN + "\\" + folderName;
                                }
                                
                                // copy !
                                CopyFolder(realPathFile, destination);
                            }









                        } // foreach files to send

                    }
                    catch (Exception Ex)
                    {
                        //erreurs.Add(name + " : " + Ex.Message);
                        log("[SEND] General ERROR : " + Ex.Message, false);
                    }

                    // update progress bar
                    userProgressBar.Value = userCount;

                } // end for each files in send box

            } // end of impersonator

        }





        // On SEND progress
        private void bwSend_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            userProgressBar.Value = e.ProgressPercentage;
        }





        // On SEND Complete
        private void bwSend_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

            BT_distribuer.Enabled = true;

            // summary messagebox
            MessageBox.Show(fileView.Items.Count + " fichier(s) distribué(s) à " + sendToList.Count + " élève(s)", "Information", MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
            log("[SEND] End of send function, seeya !", true);
        }





        // recursive copy folder to folder (2nd way)
        public static void CopyFolder(string sourceFolder, string destFolder)
        {
            if (!Directory.Exists(destFolder))
                Directory.CreateDirectory(destFolder);

            string[] files = Directory.GetFiles(sourceFolder);
            foreach (string file in files)
            {
                string name = Path.GetFileName(file);
                string dest = Path.Combine(destFolder, name);
                File.Copy(file, dest);
            }
            string[] folders = Directory.GetDirectories(sourceFolder);
            foreach (string folder in folders)
            {
                string name = Path.GetFileName(folder);
                string dest = Path.Combine(destFolder, name);
                if (!Directory.Exists(dest)) Directory.CreateDirectory(dest);
                CopyFolder(folder, dest);
            }
        }



        // Send files !
        private void BT_distribuer_Click(object sender, EventArgs e)
        {

            // progress bar min&max
            userProgressBar.Minimum = 0;
            userProgressBar.Maximum = sendToList.Count;

            if (!bwSend.IsBusy)
            {
                BT_distribuer.Enabled = false;
                
                // The display name of the logged in user
                string loggedUserDN = System.DirectoryServices.AccountManagement.UserPrincipal.Current.DisplayName;

                // Use the surname + name if no display name or login name if no option
                if (loggedUserDN == null) {
                    if (System.DirectoryServices.AccountManagement.UserPrincipal.Current.Surname != null)
                    {
                        loggedUserDN = System.DirectoryServices.AccountManagement.UserPrincipal.Current.Surname + " " + System.DirectoryServices.AccountManagement.UserPrincipal.Current.GivenName;
                    }
                    else {
                        loggedUserDN = Environment.UserName;
                    }
                }

                log("[SEND] SEND files with [" + loggedUserDN + "] user", false);
         
            
                // If users selected
                if (sendToList.Count > 0)
                {
                    string domain = EncDec.Decrypt(myconfig.serverLogin, PasswordHash).Split('\\')[0];
                    string username = EncDec.Decrypt(myconfig.serverLogin, PasswordHash).Split('\\')[1];
                    string password = EncDec.Decrypt(myconfig.serverPassword, PasswordHash);

                    log("[SEND] Try to impersonate " + username + " on " + domain, true);

                    Tuple<Dictionary<string, string>, string, ListView> mytuple = new Tuple<Dictionary<string, string>, string, ListView>(sendToList, loggedUserDN, fileView);

                    // Do the job
                    bwSend.RunWorkerAsync(mytuple);


                        /*
                        // if any error
                        if (erreurs.Count > 0)
                        {
                            string erreur_texte = "";

                            foreach (string err in erreurs)
                            {
                                erreur_texte += err;
                            }

                            MessageBox.Show(erreur_texte, "Liste des erreurs", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                        } // end if errors
                        */



                } // End if users selected

                else
                {
                    MessageBox.Show("Pas d'utilisateur selectionné", "Attention", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Exclamation);
                    log("[SEND] No user selection", true);
                }

            } //endif worker isbusy

        }





        #endregion





        #region PICK UP FILES





        // recursive copy folder to folder
        private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs, Dictionary<string, string> options)
        {

            Console.WriteLine("start copy");

            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException("Source directory does not exist or could not be found: " + sourceDirName);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();
            // If the destination directory doesn't exist, create it.
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();

            // filters
            string filter_date = "";
            string filter_name = "";
            string filter_extension = "";
            string delete_files = "";

            options.TryGetValue("extension", out filter_extension).ToString();
            options.TryGetValue("name", out filter_name).ToString();
            options.TryGetValue("deleteFiles", out delete_files).ToString();

            string mydate = options.TryGetValue("date", out filter_date).ToString();
            DateTime dateRamassage = DateTime.Parse(filter_date);

            // files to copy
            foreach (FileInfo file in files)
            {

                bool copy_ext, copy_name, copy_date;

                logFile("[TAKE] Working on file : " + file.FullName, true);

                // Filter the files

                // On extension
                if (file.Extension == "." + filter_extension || filter_extension == "")
                {
                    copy_ext = true;
                }
                else { copy_ext = false; }



                // On filename
                if (file.Name.IndexOf(filter_name, StringComparison.OrdinalIgnoreCase) >= 0 || filter_name == "")
                {
                    copy_name = true;
                }
                else { copy_name = false; }



                // On date
                if (dateRamassage.CompareTo(file.LastWriteTime) == -1)
                {
                    copy_date = true;
                }
                else { copy_date = false; }



                Console.WriteLine("name:" + copy_name + " date:" + copy_date + " ext:" + copy_ext);

                if (copy_ext == true && copy_date == true && copy_name == true)
                {
                    string temppath = Path.Combine(destDirName, file.Name);

                    Console.WriteLine("copy of : " + temppath);
                    logFile("[TAKE] Copy file " + temppath, true);

                    file.CopyTo(temppath, true);


                    // DELETE files in user folder
                    if (delete_files == "True")
                    {
                        file.Delete();
                        logFile("[TAKE] Remove file " + file.FullName + " in source folder", true);
                    }


                }

            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs, options);
                }
            }
        }





        // Do the pick up job
        private void bwPick_DoWork(object sender, DoWorkEventArgs e)
        {
            // Progress bar init
            int userCount = 0;

            BackgroundWorker worker = (BackgroundWorker)sender;
            
            // not cool, but hey, it is workin'
            Control.CheckForIllegalCrossThreadCalls = false;

            // Tuple to pack the arguments
            Tuple<Dictionary<string, string>, string, string> mytuple = (Tuple<Dictionary<string, string>, string, string>)e.Argument;
            Dictionary<string, string> sendToList = mytuple.Item1;
            string loggedUserDN = mytuple.Item2;
            string rootfolder = mytuple.Item3;

            // Impersonator params
            string domain = EncDec.Decrypt(myconfig.serverLogin, PasswordHash).Split('\\')[0];
            string username = EncDec.Decrypt(myconfig.serverLogin, PasswordHash).Split('\\')[1];
            string password = EncDec.Decrypt(myconfig.serverPassword, PasswordHash);

            // Execute job with some other creds
            using (new Impersonator(username, domain, password))
            {
                ArrayList erreurs = new ArrayList();

                // for each users in list
                foreach (KeyValuePair<string, string> user in sendToList)
                {

                    userCount++;

                    string homedir = user.Key;
                    string fullname = user.Value;
                    string group = fullname.Split('/')[0];
                    string name = fullname.Split('/')[1];
                    string folder = rootfolder + "\\";

                    log("[TAKE] Processing user : " + name + " in : " + homedir, true);

                    try
                    {
                        
                        // Create the group folder (or not)
                        if (CHKgroupSubFolder.Checked)
                        {
                            folder = rootfolder + "\\" + group;
                            Directory.CreateDirectory(folder);
                            log("[TAKE] Creating  group folder : " + group, true);
                        }
                        else
                        {
                            folder = folder + "\\";
                        }
                        
                        

                        // Create the user folder (or not)
                        if (!Directory.Exists(folder + "\\" + name))
                        {
                            if (CHKsubfolder.Checked)
                            {
                                folder = folder + "\\" + name;
                                Directory.CreateDirectory(folder);
                                log("[TAKE] Creating  username folder : " + name, true);
                            }

                        }

                        // get the folder taumata files list
                        string sourceDir = "";
                        
                        if (Tfolder.Text != "")
                        {
                            sourceDir = homedir + "\\"+ myconfig.distriFolder + "\\" + loggedUserDN + "\\" + Tfolder.Text;
                        }
                        else
                        {
                            sourceDir = homedir + "\\"+ myconfig.distriFolder + "\\" + loggedUserDN;
                        }

                        
                        // Destination folder
                        string destDir = folder;

                        
                        /*if (CHKsubfolder.Checked)
                        {
                            destDir = rootfolder + "\\" + name;
                        }

                        // No subfolder
                        else
                        {
                            destDir = rootfolder;
                        }*/


                        // Copy options
                        Dictionary<string, string> options = new Dictionary<string, string>();

                        options.Add("extension", Textension.Text);
                        options.Add("name", Tlike.Text);
                        options.Add("date", Tdate.Text);
                        options.Add("deleteFiles", CHKsupprfiles.Checked.ToString());


                        // COPY !!!!!
                        DirectoryCopy(sourceDir, destDir, true, options);


                    }
                    catch (Exception Ex)
                    {
                        erreurs.Add(name + " : " + Ex.Message);
                        log("[TAKE] General Error : " + Ex.Message, false);
                    }


                    // update progress bar
                    userProgressBar.Value = userCount;


                } // end for each users


                // En cas d'erreur
                if (erreurs.Count > 0)
                {

                    string erreur_texte = "";

                    foreach (string err in erreurs)
                    {
                        erreur_texte += err;
                    }

                    MessageBox.Show(erreur_texte, "Liste des erreurs", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    log("Erreurs constatées : " + erreur_texte, false);
                }


            } // end of impersonator

        }





        // on pick up progress
        private void bwPick_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }





        // On pick up complete
        private void bwPick_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            BT_ramasser.Enabled = true;

            // summary messagebox
            MessageBox.Show("Fichier(s) ramassé(s) pour " + sendToList.Count + " élève(s)", "Information", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
            log("[TAKE] End of Pick up function, seeya !", true);
        }





        //////////////////////////////////////////////////////////////
        // take files from users folders + filters
        //////////////////////////////////////////////////////////////
        private void BT_ramasser_Click(object sender, EventArgs e)
        {


            // If users selected
            if (sendToList.Count > 0)
            {


                BT_ramasser.Enabled = false;

                // progress bar min&max
                userProgressBar.Minimum = 0;
                userProgressBar.Maximum = sendToList.Count;


                log("[TAKE] Getting files from homedirs", false);

                // The display name of the logged in user
                string loggedUserDN = System.DirectoryServices.AccountManagement.UserPrincipal.Current.DisplayName;


                // Use the surname + name if no display name
                if (loggedUserDN == null)
                {
                    if (System.DirectoryServices.AccountManagement.UserPrincipal.Current.Surname != null)
                    {
                        loggedUserDN = System.DirectoryServices.AccountManagement.UserPrincipal.Current.Surname + " " + System.DirectoryServices.AccountManagement.UserPrincipal.Current.GivenName;
                    }
                    else
                    {
                        loggedUserDN = Environment.UserName;
                    }
                }


                log("[TAKE] Getting files of user [" + loggedUserDN + "]", false);


                DialogResult result = folderBrowser.ShowDialog();


                if (result == DialogResult.OK)
                {

                    string realPathFile;

                    // if use the bind of the homedir
                    if (Path.GetPathRoot(folderBrowser.SelectedPath) == "U:\\")
                    {
                        realPathFile = folderBrowser.SelectedPath.Replace("U:\\", System.DirectoryServices.AccountManagement.UserPrincipal.Current.HomeDirectory + "\\");
                        log("[SEND] Replacing U: by " + realPathFile, true);
                    }
                    else
                    {
                        realPathFile = folderBrowser.SelectedPath;
                    }

                    log("[SEND] Real path " + realPathFile, true);


                    // Create "taumata" (or whatever) folder
                    if (!Directory.Exists(realPathFile + "\\" + myconfig.distriFolder))
                    {
                        Directory.CreateDirectory(realPathFile + "\\" + myconfig.distriFolder);

                        log("[TAKE] creating "+ myconfig.distriFolder +" FOLDER in " + realPathFile, false);
                    }






                    // Create "Nom du devoir" subfolder
                    if (!Directory.Exists(realPathFile + "\\"+ myconfig.distriFolder + "\\" + Tnomdevoir.Text) && Tnomdevoir.Text != "")
                    {
                        Directory.CreateDirectory(realPathFile + "\\" + myconfig.distriFolder + "\\" + Tnomdevoir.Text);

                        log("[TAKE] creating " + Tnomdevoir.Text + " subfolder", false);
                    }

                    // Folder where to put all the crappy work
                    string rootfolder;
                    string outputFolder;

                    log("[TAKE] original root folder : " + realPathFile, true);

                    // fix U:\ folder 
                    if (realPathFile.Substring(realPathFile.Length - 1, 1) == "\\")
                    {
                        outputFolder = realPathFile.Remove(realPathFile.Length - 1); ;
                    }
                    else
                    {
                        outputFolder = realPathFile;
                    }




                    log("[TAKE] new root folder : " + outputFolder, true);

                    if (Tnomdevoir.Text == "")
                    {
                        rootfolder = outputFolder + "\\" + myconfig.distriFolder;
                    }
                    else
                    {
                        rootfolder = outputFolder + "\\"+ myconfig.distriFolder + "\\" + Tnomdevoir.Text;
                    }


                    log("[TAKE] Copy files in : " + rootfolder, true);

                    // go

                    string domain = EncDec.Decrypt(myconfig.serverLogin, PasswordHash).Split('\\')[0];
                    string username = EncDec.Decrypt(myconfig.serverLogin, PasswordHash).Split('\\')[1];
                    string password = EncDec.Decrypt(myconfig.serverPassword, PasswordHash);

                    log("[TAKE] try to impersonate : " + username + " on " + domain + " domain", true);



                    Tuple<Dictionary<string, string>, string, string> mytuple = new Tuple<Dictionary<string, string>, string, string>(sendToList, loggedUserDN, rootfolder);

                    // Do the job
                    bwPick.RunWorkerAsync(mytuple);

                } // endif click ok
                else {
                    BT_ramasser.Enabled = true;
                }
            }
            else
            {
                MessageBox.Show("Pas d'utilisateur selectionné", "Attention", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Exclamation);
                log("[TAKE] No user selection", true);
                BT_ramasser.Enabled = true;
            }

            log("[TAKE] End of Get Function, seeya", true);
        }


        #endregion





        #region LOGS

        //////////////////////////////////////////////////////////////
        //      Log something in logbox
        //////////////////////////////////////////////////////////////
        public void log(string txt, bool debug) {
            TBlogs.AppendText(txt + "\n");
            Console.WriteLine(txt);
            logFile(txt, debug);
        }

        
        //////////////////////////////////////////////////////////////
        //      Log something in file taumata.log
        //////////////////////////////////////////////////////////////
        public static void logFile(string txt, bool debug)
        {
            StreamWriter sr = new StreamWriter("taumata.log", true, System.Text.Encoding.UTF8);
            
            
            // normal logging
            if (debug == false)
            {
                sr.WriteLine(DateTime.Now + " " + txt);
            }

            // debug logging
            else {
                // debug mode
                if (myconfig.debugMode == "1")
                {
                    sr.WriteLine(DateTime.Now + " " + txt);
                }
            }
                       

            sr.Close();

            
        }

        #endregion


        #region LIBS

        // get a folder size
        static long getFolderSize (string path) {

            string[] arr = Directory.GetFiles(path, "*.*");

            long sum = 0;

            foreach(string file in arr)
            {
                sum += new FileInfo(file).Length;
            }


            return sum;
        }


        // get the Fileview size sum (files and folder)
        private long getFileviewSize(ListView LV)
        {

            long sum = 0;

            if (LV.Items.Count == 0)
            {
                return 0;
            }

            foreach (ListViewItem item in LV.Items)
            {

                if (item.Text.Substring(0, 9) == "[FICHIER]")
                {
                    sum += new FileInfo(item.ImageKey).Length;
                }

                if (item.Text.Substring(0, 9) == "[DOSSIER]")
                {
                    sum += getFolderSize(item.ImageKey);
                }
            }

            return sum;
        }


        #endregion



        #region VALIDATIONS

        //////////////////////////////////////////////////////////////
        // VALIDATIONS
        //////////////////////////////////////////////////////////////

        // Illegal characters validation for destination folder creation
        private void TdestFolder_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.ToString().IndexOfAny(new char[] { '\\', '/', ':', '*', '?', '"', '<', '>', '|' }) != -1)
            {
                e.KeyChar = (char)0;
                MessageBox.Show("Caractère interdit !");
            }

            
        }
        
        // Illegal characters validation for collect folder creation
        private void Tnomdevoir_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.ToString().IndexOfAny(new char[] { '\\', '/', ':', '*', '?', '"', '<', '>', '|' }) != -1)
            {
                e.KeyChar = (char)0;
                MessageBox.Show("Caractère interdit !");
            }
        }

        #endregion






        #region GROUPS

        //////////////////////////////////////////////////////////////
        // SAVE USER SELECTION IN GROUP
        //////////////////////////////////////////////////////////////
        private void saveGroup_Click(object sender, EventArgs e)
        {
            //Console.WriteLine(sendToList.Count);

            Console.WriteLine("export group");

            // get groups name list
            List<string> groupList = new List<string>();
            

            foreach (Dictionary<string, Dictionary<string, string>> group in customParams.groups)
            {

                Console.WriteLine(group.Keys);

                foreach (KeyValuePair<string, Dictionary<string, string>> item in group)
                {
                    Console.WriteLine(item.Key);
                    groupList.Add(item.Key);
                }

            }



            if (sendToList.Count > 0)
            {

                // Inputbox
                string result = Microsoft.VisualBasic.Interaction.InputBox("Le nom du groupe", "Groupe");


                if (result.Trim() != "" && !groupList.Contains(result.Trim()))
                {
                    Console.WriteLine("input box : " + result);

                    customParams.subFolderName = "yeah";

                    customParams.addGroup(result.Trim(), sendToList);

                    customParams.exportJSON(Environment.UserName);

                    // reload list
                    customParams = customParams.loadCustomizeFile(Environment.UserName);
                    loadGroupsList(customParams);

                }
                else {
                    MessageBox.Show("Le nom du groupe est vide ou la liste contient déjà ce nom !");
                }
            }
            else {
                MessageBox.Show("Pas d'utilisateur selectionné");
            }
            
            
        }


        //////////////////////////////////////////////////////////////
        // DELETE GROUP
        //////////////////////////////////////////////////////////////
        private void deleteGroup_Click(object sender, EventArgs e)
        {
            if (listGroups.SelectedItems.Count > 0)
            {

                // Bla bla are you sure ?
                
                // Delete
                customParams.deleteGroup(listGroups.SelectedItem.ToString());

                // Save
                customParams.exportJSON(Environment.UserName);

                // reload list
                loadGroupsList(customParams);


            }
        }



        //////////////////////////////////////////////////////////////
        // LOAD SELECTION
        //////////////////////////////////////////////////////////////
        private void loadGroup_Click(object sender, EventArgs e)
        {

            Console.WriteLine(listGroups.SelectedItem);
                
            if (listGroups.SelectedItems.Count > 0)
            {

                // unckeck all nodes
                foreach (TreeNode node in TV_groups.Nodes) {
                    node.Checked = false;
                }

                // get the group members
                Dictionary<string, string> groupMembers = new Dictionary<string, string>();
                groupMembers = customParams.getGroupMembers(listGroups.SelectedItem.ToString());

                    // check ldap users
                    foreach (KeyValuePair<string, string> member in groupMembers)
                    {

                        try
                        {
                            // find users to check
                            TreeNode[] tt = TV_groups.Nodes.Find(member.Key, true);

                            // check them !
                            tt[0].Checked = true;

                            // highlight them !
                            TV_groups.SelectedNode = tt[0];
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Load a group error : " + ex.Message);
                        }

                    }

        

            }
            else {
                MessageBox.Show("Vous devez selectionner un groupe");
            }




        }


        //////////////////////////////////////////////////////////////
        // FILL LIST WITH GROUPS
        //////////////////////////////////////////////////////////////
        private void loadGroupsList(Customize customParams) {

            // Clear the list
            listGroups.Items.Clear();


            foreach (Dictionary<string, Dictionary<string, string>> group in customParams.groups)
            {

                Console.WriteLine(group.Keys);

                foreach (KeyValuePair<string, Dictionary<string, string>> item in group)
                {
                    Console.WriteLine(item.Key);
                    listGroups.Items.Add(item.Key);
                }

            }
        
        }




        #endregion




        #region LINKS 


        //////////////////////////////////////////////////////////////
        // Links
        //////////////////////////////////////////////////////////////
        private void linkWikipedia_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

            System.Diagnostics.Process.Start("https://fr.wikipedia.org/wiki/Taumata%C2%ADwhakatangihanga%C2%ADkoauau%C2%ADo%C2%ADtamatea%C2%ADturi%C2%ADpukaka%C2%ADpiki%C2%ADmaungah%C2%ADoronuku%C2%ADpokai%C2%ADwhenuaki%C2%ADtanatahu");
        }

        private void linkOfficial_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://gitlab.com/dividi/taumata");
        }

        private void linkManual_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://gitlab.com/dividi/taumata/wikis/home");
        }








        #endregion

    } // end of partial class form1



    #region Customize Class Definition

    ////////////////////////////////////////////////////////////////////////
    //
    // Custom config class (to ... customize the user xperience !)
    //
    ////////////////////////////////////////////////////////////////////////
    public class Customize
    {
        public string subFolderName;
        public List<Dictionary<string, Dictionary<string, string>>> groups = new List<Dictionary<string, Dictionary<string, string>>>();

        // Methods

        public Customize() {}



        // delete a group
        // change an option


        // add a group (beware conflicts in names)
        public bool addGroup (string name, Dictionary<string,string> members) {

            // the group
            Dictionary<string, Dictionary<string,string>> group = new Dictionary<string,Dictionary<string,string>>();

            group.Add(name, members);


            groups.Add(group);

            return true;
            

        } // end addgroup



        // delete a group (boom !)
        public bool deleteGroup(string groupName)
        {
            // delete group
            foreach (Dictionary<string, Dictionary<string, string>> group in groups)
            {
                group.Remove(groupName);
            }

            // delete container (to avoid empty shells)
            foreach (Dictionary<string, Dictionary<string, string>> group in groups)
            {
                Console.WriteLine(group.Count);

                if (group.Count == 0) {
                    groups.Remove(group);
                    return true;
                }

            } // end delete container


            return false;


        } // end addgroup




        // add a group (beware conflicts in names)
        public Customize loadCustomizeFile(string userName)
        {

            // Load customize file
            if (File.Exists(userName + ".json"))
            {
                // deszrialize the JSON file
                StreamReader sr = new StreamReader(userName + ".json", Encoding.UTF8);
                Customize customParams = JsonConvert.DeserializeObject<Customize>(sr.ReadToEnd());
                sr.Close();

                return customParams;
            }
            else {
                return null;
            }

            


        } // end loadCustomizeFile
       



        // Get group members
        public Dictionary<string, string> getGroupMembers(string groupName) {

            Dictionary<string, string> members = new Dictionary<string, string>();


            // In each groups items, find if the dictionary key is good
            foreach (Dictionary<string, Dictionary<string, string>> group in groups)
            {
                if ( group.TryGetValue(groupName, out members) )
                {
                    return members;
                }
            }

            return members;
        } // end getgroupmembers




        // export the object to JSON file
        public void exportJSON(string userName) {

            StreamWriter sr = new StreamWriter(userName + ".json", false, System.Text.Encoding.UTF8);

            // serialize all the groups        
            sr.WriteLine(JsonConvert.SerializeObject(this));

            // close the streamwriter
            sr.Close();
        
        
        } // end of exportJSON



    } // end class

    #endregion


} // end form1






    #region references

/*
  Task factory : https://msdn.microsoft.com/en-us/library/system.threading.tasks.taskfactory.aspx
 */










// Instead of impersonator :
/*
using (new NetworkConnection(@"\\server\read", readCredentials))
using (new NetworkConnection(@"\\server2\write", writeCredentials)) {
   File.Copy(@"\\server\read\file", @"\\server2\write\file");
}
*/


/*
 //background worker
        private void button1_Click(object sender, EventArgs e)
        {
            if (!bworker.IsBusy) {
                button1.Enabled = false;
                bworker.RunWorkerAsync();
            }


        }

        private void bworker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = (BackgroundWorker)sender;
            for (int i = 0; i < 100; ++i)
            {
                // report your progres
                worker.ReportProgress(i);

                // pretend like this a really complex calculation going on eating up CPU time
                System.Threading.Thread.Sleep(100);
            }
            e.Result = "42";






        }

        private void bworker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            Linfo.Text = e.ProgressPercentage.ToString() + "% complete";
        }

        private void bworker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Linfo.Text = "The answer is: " + e.Result.ToString();
            button1.Enabled = true;
        }*/

#endregion