﻿namespace LDAP
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.TV_groups = new System.Windows.Forms.TreeView();
            this.fileBrowser = new System.Windows.Forms.OpenFileDialog();
            this.Tselected = new System.Windows.Forms.Label();
            this.folderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            this.logs = new System.Windows.Forms.TabPage();
            this.TBlogs = new System.Windows.Forms.TextBox();
            this.ramasser = new System.Windows.Forms.TabPage();
            this.CHKgroupSubFolder = new System.Windows.Forms.CheckBox();
            this.CHKsupprfiles = new System.Windows.Forms.CheckBox();
            this.CHKsubfolder = new System.Windows.Forms.CheckBox();
            this.Tdate = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.Tnomdevoir = new System.Windows.Forms.TextBox();
            this.Textension = new System.Windows.Forms.TextBox();
            this.Tfolder = new System.Windows.Forms.TextBox();
            this.Tlike = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.BT_ramasser = new System.Windows.Forms.Button();
            this.distribuer = new System.Windows.Forms.TabPage();
            this.openFolderBrowser = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.TdestFolder = new System.Windows.Forms.TextBox();
            this.Tsuppr = new System.Windows.Forms.Button();
            this.Bvider = new System.Windows.Forms.Button();
            this.fileView = new System.Windows.Forms.ListView();
            this.openFileBrowser = new System.Windows.Forms.Button();
            this.BT_distribuer = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.loadGroup = new System.Windows.Forms.Button();
            this.listGroups = new System.Windows.Forms.ListBox();
            this.deleteGroup = new System.Windows.Forms.Button();
            this.saveGroup = new System.Windows.Forms.Button();
            this.Aide = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.linkManual = new System.Windows.Forms.LinkLabel();
            this.linkOfficial = new System.Windows.Forms.LinkLabel();
            this.linkWikipedia = new System.Windows.Forms.LinkLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tTip = new System.Windows.Forms.ToolTip(this.components);
            this.bwSend = new System.ComponentModel.BackgroundWorker();
            this.bwPick = new System.ComponentModel.BackgroundWorker();
            this.userProgressBar = new System.Windows.Forms.ProgressBar();
            this.sendFolderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.BT_showFolder = new System.Windows.Forms.Button();
            this.logs.SuspendLayout();
            this.ramasser.SuspendLayout();
            this.distribuer.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.Aide.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // TV_groups
            // 
            this.TV_groups.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TV_groups.CheckBoxes = true;
            this.TV_groups.Location = new System.Drawing.Point(12, 12);
            this.TV_groups.Name = "TV_groups";
            this.TV_groups.Size = new System.Drawing.Size(303, 406);
            this.TV_groups.TabIndex = 0;
            this.TV_groups.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.TV_groups_AfterCheck);
            // 
            // fileBrowser
            // 
            this.fileBrowser.DereferenceLinks = false;
            this.fileBrowser.Multiselect = true;
            this.fileBrowser.SupportMultiDottedExtensions = true;
            this.fileBrowser.Title = "Fichier à envoyer";
            this.fileBrowser.FileOk += new System.ComponentModel.CancelEventHandler(this.fileBrowser_FileOk);
            // 
            // Tselected
            // 
            this.Tselected.AutoSize = true;
            this.Tselected.Location = new System.Drawing.Point(12, 424);
            this.Tselected.Name = "Tselected";
            this.Tselected.Size = new System.Drawing.Size(0, 13);
            this.Tselected.TabIndex = 15;
            // 
            // logs
            // 
            this.logs.Controls.Add(this.TBlogs);
            this.logs.Location = new System.Drawing.Point(4, 22);
            this.logs.Name = "logs";
            this.logs.Size = new System.Drawing.Size(312, 380);
            this.logs.TabIndex = 2;
            this.logs.Text = "logs";
            this.logs.UseVisualStyleBackColor = true;
            // 
            // TBlogs
            // 
            this.TBlogs.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBlogs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TBlogs.Location = new System.Drawing.Point(3, 3);
            this.TBlogs.Multiline = true;
            this.TBlogs.Name = "TBlogs";
            this.TBlogs.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TBlogs.Size = new System.Drawing.Size(306, 377);
            this.TBlogs.TabIndex = 13;
            this.TBlogs.WordWrap = false;
            // 
            // ramasser
            // 
            this.ramasser.Controls.Add(this.CHKgroupSubFolder);
            this.ramasser.Controls.Add(this.CHKsupprfiles);
            this.ramasser.Controls.Add(this.CHKsubfolder);
            this.ramasser.Controls.Add(this.Tdate);
            this.ramasser.Controls.Add(this.label5);
            this.ramasser.Controls.Add(this.Tnomdevoir);
            this.ramasser.Controls.Add(this.Textension);
            this.ramasser.Controls.Add(this.Tfolder);
            this.ramasser.Controls.Add(this.Tlike);
            this.ramasser.Controls.Add(this.label4);
            this.ramasser.Controls.Add(this.label3);
            this.ramasser.Controls.Add(this.label2);
            this.ramasser.Controls.Add(this.label1);
            this.ramasser.Controls.Add(this.BT_ramasser);
            this.ramasser.Location = new System.Drawing.Point(4, 22);
            this.ramasser.Name = "ramasser";
            this.ramasser.Padding = new System.Windows.Forms.Padding(3);
            this.ramasser.Size = new System.Drawing.Size(312, 380);
            this.ramasser.TabIndex = 1;
            this.ramasser.Text = "Ramasser";
            this.ramasser.UseVisualStyleBackColor = true;
            // 
            // CHKgroupSubFolder
            // 
            this.CHKgroupSubFolder.AutoSize = true;
            this.CHKgroupSubFolder.Location = new System.Drawing.Point(20, 197);
            this.CHKgroupSubFolder.Name = "CHKgroupSubFolder";
            this.CHKgroupSubFolder.Size = new System.Drawing.Size(178, 17);
            this.CHKgroupSubFolder.TabIndex = 24;
            this.CHKgroupSubFolder.Text = "Créer un sous dossier par classe";
            this.tTip.SetToolTip(this.CHKgroupSubFolder, "Permet de créer un sous dossier pou chaque classe");
            this.CHKgroupSubFolder.UseVisualStyleBackColor = true;
            // 
            // CHKsupprfiles
            // 
            this.CHKsupprfiles.AutoSize = true;
            this.CHKsupprfiles.Location = new System.Drawing.Point(20, 151);
            this.CHKsupprfiles.Name = "CHKsupprfiles";
            this.CHKsupprfiles.Size = new System.Drawing.Size(172, 17);
            this.CHKsupprfiles.TabIndex = 23;
            this.CHKsupprfiles.Text = "Supprimer les fichiers ramassés";
            this.tTip.SetToolTip(this.CHKsupprfiles, "Supprime les fichiers ramassés");
            this.CHKsupprfiles.UseVisualStyleBackColor = true;
            // 
            // CHKsubfolder
            // 
            this.CHKsubfolder.AutoSize = true;
            this.CHKsubfolder.Checked = true;
            this.CHKsubfolder.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CHKsubfolder.Location = new System.Drawing.Point(20, 174);
            this.CHKsubfolder.Name = "CHKsubfolder";
            this.CHKsubfolder.Size = new System.Drawing.Size(174, 17);
            this.CHKsubfolder.TabIndex = 22;
            this.CHKsubfolder.Text = "Créer un sous dossier par élève";
            this.CHKsubfolder.UseVisualStyleBackColor = true;
            // 
            // Tdate
            // 
            this.Tdate.Location = new System.Drawing.Point(145, 18);
            this.Tdate.MinDate = new System.DateTime(1976, 9, 16, 0, 0, 0, 0);
            this.Tdate.Name = "Tdate";
            this.Tdate.Size = new System.Drawing.Size(150, 20);
            this.Tdate.TabIndex = 21;
            this.Tdate.Tag = "";
            this.tTip.SetToolTip(this.Tdate, "Les fichiers après cette date seront ramassés");
            this.Tdate.Value = new System.DateTime(2016, 1, 1, 0, 0, 0, 0);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(21, 263);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(208, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "Ramasser les fichiers dans ce sous-dossier";
            // 
            // Tnomdevoir
            // 
            this.Tnomdevoir.Location = new System.Drawing.Point(21, 282);
            this.Tnomdevoir.MaxLength = 100;
            this.Tnomdevoir.Name = "Tnomdevoir";
            this.Tnomdevoir.Size = new System.Drawing.Size(256, 20);
            this.Tnomdevoir.TabIndex = 19;
            this.tTip.SetToolTip(this.Tnomdevoir, "Ramasser dans un dossier portant ce nom");
            this.Tnomdevoir.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Tnomdevoir_KeyPress);
            // 
            // Textension
            // 
            this.Textension.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Textension.Location = new System.Drawing.Point(145, 106);
            this.Textension.MaxLength = 4;
            this.Textension.Name = "Textension";
            this.Textension.Size = new System.Drawing.Size(51, 20);
            this.Textension.TabIndex = 18;
            this.Textension.Tag = "";
            this.tTip.SetToolTip(this.Textension, "Seuls les fichiers portant cette extension seront ramassés");
            // 
            // Tfolder
            // 
            this.Tfolder.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Tfolder.Location = new System.Drawing.Point(145, 76);
            this.Tfolder.MaxLength = 100;
            this.Tfolder.Name = "Tfolder";
            this.Tfolder.Size = new System.Drawing.Size(150, 20);
            this.Tfolder.TabIndex = 16;
            this.Tfolder.Tag = "";
            this.tTip.SetToolTip(this.Tfolder, "Seuls les fichiers dans le sous dossier portant ce nom seront ramassés");
            // 
            // Tlike
            // 
            this.Tlike.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Tlike.Location = new System.Drawing.Point(145, 46);
            this.Tlike.MaxLength = 100;
            this.Tlike.Name = "Tlike";
            this.Tlike.Size = new System.Drawing.Size(150, 20);
            this.Tlike.TabIndex = 14;
            this.Tlike.Tag = "";
            this.tTip.SetToolTip(this.Tlike, "Les fichiers contenant dans leur nom ce texte seront ramassés");
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 113);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Ayant pour extension";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Juste le sous dossier";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Ressemble à";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "A partir du";
            // 
            // BT_ramasser
            // 
            this.BT_ramasser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.BT_ramasser.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BT_ramasser.Location = new System.Drawing.Point(21, 323);
            this.BT_ramasser.Name = "BT_ramasser";
            this.BT_ramasser.Size = new System.Drawing.Size(256, 44);
            this.BT_ramasser.TabIndex = 10;
            this.BT_ramasser.Text = "Ramasser";
            this.tTip.SetToolTip(this.BT_ramasser, "Demande où ramasser et ... ramasse !");
            this.BT_ramasser.UseVisualStyleBackColor = false;
            this.BT_ramasser.Click += new System.EventHandler(this.BT_ramasser_Click);
            // 
            // distribuer
            // 
            this.distribuer.Controls.Add(this.openFolderBrowser);
            this.distribuer.Controls.Add(this.label7);
            this.distribuer.Controls.Add(this.TdestFolder);
            this.distribuer.Controls.Add(this.Tsuppr);
            this.distribuer.Controls.Add(this.Bvider);
            this.distribuer.Controls.Add(this.fileView);
            this.distribuer.Controls.Add(this.openFileBrowser);
            this.distribuer.Controls.Add(this.BT_distribuer);
            this.distribuer.Location = new System.Drawing.Point(4, 22);
            this.distribuer.Name = "distribuer";
            this.distribuer.Padding = new System.Windows.Forms.Padding(3);
            this.distribuer.Size = new System.Drawing.Size(312, 380);
            this.distribuer.TabIndex = 0;
            this.distribuer.Text = "Distribuer";
            this.distribuer.UseVisualStyleBackColor = true;
            // 
            // openFolderBrowser
            // 
            this.openFolderBrowser.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.openFolderBrowser.Location = new System.Drawing.Point(97, 6);
            this.openFolderBrowser.Name = "openFolderBrowser";
            this.openFolderBrowser.Size = new System.Drawing.Size(85, 44);
            this.openFolderBrowser.TabIndex = 14;
            this.openFolderBrowser.Tag = "";
            this.openFolderBrowser.Text = "Choisir un DOSSIER";
            this.tTip.SetToolTip(this.openFolderBrowser, "Pour choisir les fichiers à poster aux utilisateurs.");
            this.openFolderBrowser.UseVisualStyleBackColor = true;
            this.openFolderBrowser.Click += new System.EventHandler(this.openFolderBrowser_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 59);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(128, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Distribuer dans ce dossier";
            // 
            // TdestFolder
            // 
            this.TdestFolder.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TdestFolder.Location = new System.Drawing.Point(140, 56);
            this.TdestFolder.MaxLength = 100;
            this.TdestFolder.Name = "TdestFolder";
            this.TdestFolder.Size = new System.Drawing.Size(166, 20);
            this.TdestFolder.TabIndex = 12;
            this.TdestFolder.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TdestFolder_KeyPress);
            // 
            // Tsuppr
            // 
            this.Tsuppr.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Tsuppr.Location = new System.Drawing.Point(263, 30);
            this.Tsuppr.Name = "Tsuppr";
            this.Tsuppr.Size = new System.Drawing.Size(43, 20);
            this.Tsuppr.TabIndex = 11;
            this.Tsuppr.Tag = "";
            this.Tsuppr.Text = "suppr";
            this.tTip.SetToolTip(this.Tsuppr, "Supprimer les fichiers sélectionnés dans la liste des fichiers à envoyer");
            this.Tsuppr.UseVisualStyleBackColor = true;
            this.Tsuppr.Click += new System.EventHandler(this.Tsuppr_Click);
            // 
            // Bvider
            // 
            this.Bvider.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Bvider.Location = new System.Drawing.Point(263, 6);
            this.Bvider.Name = "Bvider";
            this.Bvider.Size = new System.Drawing.Size(43, 20);
            this.Bvider.TabIndex = 10;
            this.Bvider.Tag = "";
            this.Bvider.Text = "vider";
            this.tTip.SetToolTip(this.Bvider, "Vider entièrement la liste des fichiers à envoyer");
            this.Bvider.UseVisualStyleBackColor = true;
            this.Bvider.Click += new System.EventHandler(this.Bvider_Click);
            // 
            // fileView
            // 
            this.fileView.AllowDrop = true;
            this.fileView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fileView.GridLines = true;
            this.fileView.Location = new System.Drawing.Point(6, 82);
            this.fileView.Name = "fileView";
            this.fileView.Size = new System.Drawing.Size(300, 292);
            this.fileView.TabIndex = 8;
            this.fileView.Tag = "";
            this.tTip.SetToolTip(this.fileView, "Liste des fichiers à envoyer aux utilisateurs");
            this.fileView.UseCompatibleStateImageBehavior = false;
            this.fileView.View = System.Windows.Forms.View.List;
            this.fileView.DragDrop += new System.Windows.Forms.DragEventHandler(this.fileView_DragDrop);
            this.fileView.DragEnter += new System.Windows.Forms.DragEventHandler(this.fileView_DragEnter);
            // 
            // openFileBrowser
            // 
            this.openFileBrowser.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.openFileBrowser.Location = new System.Drawing.Point(6, 6);
            this.openFileBrowser.Name = "openFileBrowser";
            this.openFileBrowser.Size = new System.Drawing.Size(85, 44);
            this.openFileBrowser.TabIndex = 7;
            this.openFileBrowser.Tag = "";
            this.openFileBrowser.Text = "Choisir des FICHIERS";
            this.tTip.SetToolTip(this.openFileBrowser, "Pour choisir les fichiers à poster aux utilisateurs.");
            this.openFileBrowser.UseVisualStyleBackColor = true;
            this.openFileBrowser.Click += new System.EventHandler(this.openFileBrowser_Click);
            // 
            // BT_distribuer
            // 
            this.BT_distribuer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.BT_distribuer.Enabled = false;
            this.BT_distribuer.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BT_distribuer.Location = new System.Drawing.Point(188, 6);
            this.BT_distribuer.Name = "BT_distribuer";
            this.BT_distribuer.Size = new System.Drawing.Size(69, 44);
            this.BT_distribuer.TabIndex = 9;
            this.BT_distribuer.Tag = "";
            this.BT_distribuer.Text = "Distribuer";
            this.tTip.SetToolTip(this.BT_distribuer, "Pour distribuer le fichiers sélectionnés aux utilisateurs");
            this.BT_distribuer.UseVisualStyleBackColor = false;
            this.BT_distribuer.Click += new System.EventHandler(this.BT_distribuer_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.distribuer);
            this.tabControl1.Controls.Add(this.ramasser);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.logs);
            this.tabControl1.Controls.Add(this.Aide);
            this.tabControl1.Location = new System.Drawing.Point(321, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(320, 406);
            this.tabControl1.TabIndex = 14;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.loadGroup);
            this.tabPage1.Controls.Add(this.listGroups);
            this.tabPage1.Controls.Add(this.deleteGroup);
            this.tabPage1.Controls.Add(this.saveGroup);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(312, 380);
            this.tabPage1.TabIndex = 4;
            this.tabPage1.Text = "Groupes";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // loadGroup
            // 
            this.loadGroup.BackColor = System.Drawing.Color.LightSkyBlue;
            this.loadGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.loadGroup.Location = new System.Drawing.Point(116, 21);
            this.loadGroup.Name = "loadGroup";
            this.loadGroup.Size = new System.Drawing.Size(75, 57);
            this.loadGroup.TabIndex = 3;
            this.loadGroup.Text = "Charger le groupe";
            this.tTip.SetToolTip(this.loadGroup, "Permet de charger une sélection préalablement sauvegardée.");
            this.loadGroup.UseVisualStyleBackColor = false;
            this.loadGroup.Click += new System.EventHandler(this.loadGroup_Click);
            // 
            // listGroups
            // 
            this.listGroups.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listGroups.FormattingEnabled = true;
            this.listGroups.Location = new System.Drawing.Point(15, 99);
            this.listGroups.Name = "listGroups";
            this.listGroups.Size = new System.Drawing.Size(280, 171);
            this.listGroups.TabIndex = 2;
            // 
            // deleteGroup
            // 
            this.deleteGroup.BackColor = System.Drawing.Color.Salmon;
            this.deleteGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.deleteGroup.Location = new System.Drawing.Point(211, 21);
            this.deleteGroup.Name = "deleteGroup";
            this.deleteGroup.Size = new System.Drawing.Size(75, 57);
            this.deleteGroup.TabIndex = 1;
            this.deleteGroup.Text = "Supprimer le groupe";
            this.tTip.SetToolTip(this.deleteGroup, "Permet de supprimer une sélection sauvegardée");
            this.deleteGroup.UseVisualStyleBackColor = false;
            this.deleteGroup.Click += new System.EventHandler(this.deleteGroup_Click);
            // 
            // saveGroup
            // 
            this.saveGroup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.saveGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.saveGroup.Location = new System.Drawing.Point(23, 21);
            this.saveGroup.Name = "saveGroup";
            this.saveGroup.Size = new System.Drawing.Size(75, 57);
            this.saveGroup.TabIndex = 0;
            this.saveGroup.Text = "Sauver la sélection";
            this.tTip.SetToolTip(this.saveGroup, "Cliquer sur ce bouton permet de mémoriser la sélection des utilisateurs.");
            this.saveGroup.UseVisualStyleBackColor = false;
            this.saveGroup.Click += new System.EventHandler(this.saveGroup_Click);
            // 
            // Aide
            // 
            this.Aide.Controls.Add(this.label6);
            this.Aide.Controls.Add(this.linkManual);
            this.Aide.Controls.Add(this.linkOfficial);
            this.Aide.Controls.Add(this.linkWikipedia);
            this.Aide.Controls.Add(this.pictureBox1);
            this.Aide.Controls.Add(this.textBox2);
            this.Aide.Controls.Add(this.textBox1);
            this.Aide.Location = new System.Drawing.Point(4, 22);
            this.Aide.Name = "Aide";
            this.Aide.Size = new System.Drawing.Size(312, 380);
            this.Aide.TabIndex = 3;
            this.Aide.Text = "Aide";
            this.Aide.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(0, 365);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(299, 12);
            this.label6.TabIndex = 9;
            this.label6.Text = "Merci à Rémi, Hélène, Jibouille et Céline pour le débugage, les idées... :)";
            // 
            // linkManual
            // 
            this.linkManual.AutoSize = true;
            this.linkManual.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkManual.Location = new System.Drawing.Point(69, 295);
            this.linkManual.Name = "linkManual";
            this.linkManual.Size = new System.Drawing.Size(146, 24);
            this.linkManual.TabIndex = 8;
            this.linkManual.TabStop = true;
            this.linkManual.Text = "Manuel en ligne";
            this.linkManual.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkManual_LinkClicked);
            // 
            // linkOfficial
            // 
            this.linkOfficial.AutoSize = true;
            this.linkOfficial.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkOfficial.Location = new System.Drawing.Point(95, 255);
            this.linkOfficial.Name = "linkOfficial";
            this.linkOfficial.Size = new System.Drawing.Size(102, 24);
            this.linkOfficial.TabIndex = 7;
            this.linkOfficial.TabStop = true;
            this.linkOfficial.Text = "Site Officiel";
            this.linkOfficial.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkOfficial_LinkClicked);
            // 
            // linkWikipedia
            // 
            this.linkWikipedia.AutoSize = true;
            this.linkWikipedia.Location = new System.Drawing.Point(201, 117);
            this.linkWikipedia.Name = "linkWikipedia";
            this.linkWikipedia.Size = new System.Drawing.Size(88, 17);
            this.linkWikipedia.TabIndex = 5;
            this.linkWikipedia.TabStop = true;
            this.linkWikipedia.Text = "source wikipédia";
            this.linkWikipedia.UseCompatibleTextRendering = true;
            this.linkWikipedia.VisitedLinkColor = System.Drawing.Color.Blue;
            this.linkWikipedia.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkWikipedia_LinkClicked);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(116, 142);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(68, 69);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.White;
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(0, 10);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(197, 29);
            this.textBox2.TabIndex = 1;
            this.textBox2.Text = "Taumata[...]anatahu";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.White;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Location = new System.Drawing.Point(4, 39);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(305, 103);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = resources.GetString("textBox1.Text");
            // 
            // tTip
            // 
            this.tTip.IsBalloon = true;
            // 
            // bwSend
            // 
            this.bwSend.WorkerReportsProgress = true;
            this.bwSend.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwSend_DoWork);
            this.bwSend.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bwSend_ProgressChanged);
            this.bwSend.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwSend_RunWorkerCompleted);
            // 
            // bwPick
            // 
            this.bwPick.WorkerReportsProgress = true;
            this.bwPick.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwPick_DoWork);
            this.bwPick.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bwPick_ProgressChanged);
            this.bwPick.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwPick_RunWorkerCompleted);
            // 
            // userProgressBar
            // 
            this.userProgressBar.Location = new System.Drawing.Point(321, 424);
            this.userProgressBar.Name = "userProgressBar";
            this.userProgressBar.Size = new System.Drawing.Size(318, 20);
            this.userProgressBar.TabIndex = 17;
            // 
            // sendFolderBrowser
            // 
            this.sendFolderBrowser.ShowNewFolderButton = false;
            // 
            // BT_showFolder
            // 
            this.BT_showFolder.Location = new System.Drawing.Point(240, 424);
            this.BT_showFolder.Name = "BT_showFolder";
            this.BT_showFolder.Size = new System.Drawing.Size(75, 23);
            this.BT_showFolder.TabIndex = 18;
            this.BT_showFolder.Text = "voir";
            this.BT_showFolder.UseVisualStyleBackColor = true;
            this.BT_showFolder.Click += new System.EventHandler(this.BT_showFolder_Click);
            // 
            // Form1
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(650, 453);
            this.Controls.Add(this.BT_showFolder);
            this.Controls.Add(this.userProgressBar);
            this.Controls.Add(this.Tselected);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.TV_groups);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Taumata... v20190122";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.logs.ResumeLayout(false);
            this.logs.PerformLayout();
            this.ramasser.ResumeLayout(false);
            this.ramasser.PerformLayout();
            this.distribuer.ResumeLayout(false);
            this.distribuer.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.Aide.ResumeLayout(false);
            this.Aide.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView TV_groups;
        private System.Windows.Forms.OpenFileDialog fileBrowser;
        private System.Windows.Forms.Label Tselected;
        private System.Windows.Forms.FolderBrowserDialog folderBrowser;
        private System.Windows.Forms.TabPage logs;
        private System.Windows.Forms.TextBox TBlogs;
        private System.Windows.Forms.TabPage ramasser;
        private System.Windows.Forms.DateTimePicker Tdate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox Tnomdevoir;
        private System.Windows.Forms.TextBox Textension;
        private System.Windows.Forms.TextBox Tfolder;
        private System.Windows.Forms.TextBox Tlike;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BT_ramasser;
        private System.Windows.Forms.TabPage distribuer;
        private System.Windows.Forms.Button Tsuppr;
        private System.Windows.Forms.Button Bvider;
        private System.Windows.Forms.ListView fileView;
        private System.Windows.Forms.Button openFileBrowser;
        private System.Windows.Forms.Button BT_distribuer;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.ToolTip tTip;
        private System.Windows.Forms.CheckBox CHKsubfolder;
        private System.Windows.Forms.CheckBox CHKsupprfiles;
        private System.Windows.Forms.TabPage Aide;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.LinkLabel linkWikipedia;
        private System.Windows.Forms.LinkLabel linkManual;
        private System.Windows.Forms.LinkLabel linkOfficial;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TdestFolder;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.ListBox listGroups;
        private System.Windows.Forms.Button deleteGroup;
        private System.Windows.Forms.Button saveGroup;
        private System.Windows.Forms.Button loadGroup;
        private System.ComponentModel.BackgroundWorker bwSend;
        private System.ComponentModel.BackgroundWorker bwPick;
        private System.Windows.Forms.ProgressBar userProgressBar;
        private System.Windows.Forms.Button openFolderBrowser;
        private System.Windows.Forms.FolderBrowserDialog sendFolderBrowser;
        private System.Windows.Forms.CheckBox CHKgroupSubFolder;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Button BT_showFolder;
    }
}

