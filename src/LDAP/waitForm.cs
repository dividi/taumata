﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LDAP
{


    public partial class waitForm : Form
    {

        static Random _random = new Random();

        public waitForm()
        {
            InitializeComponent();
        }



        // get a pun line
        public string getLine ()
        {
            List<string> part1 = new List<string> { "Cartographie", "Compilation", "Réorganisation", "Vectorisation", "Elevation", "Morcelage", "Chargement", "Sauvegarde", "Négociation", "Destruction", "Activation", "Hashage", "Salage", "Modification", "Extinction", "Shutdown", "Reset", "Réinitialisation", "Initialisation", "Randomization", "Fichage", "Décuplage", "Modération", "Import", "Export" };
            List<string> part2 = new List<string> { "inter-thread", "des azimuts", "des groupes", "des utilisateurs", "asymétrique des valeurs", "kinétique", "en boucle", "des champs itératifs", "hebdomadaire", "logique", "physique", "des données abstraites", "des usages concrets", "du 802.1x en cours", "du réseau non authentifié", "des superlatifs", "des aléats", "des blocs logiques", "tellurique", "de la clé antépénultième" };

            int index1 = _random.Next(part1.Count);
            int index2 = _random.Next(part2.Count);

            return part1[index1] + " " + part2[index2] + "...";
        }


        // each tick of timer
        private void timer1_Tick(object sender, EventArgs e)
        {
            this.Text = getLine();
        }
    }
}
