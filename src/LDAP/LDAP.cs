﻿/*
 * TAUMATA
 * 
 *  LDAP CLASS
 *  DAVID LE VIAVANT 2016
 *  LICENCE GPL3
 * 
 * */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using System.IO;
using System.Text.RegularExpressions;
using Newtonsoft.Json;


namespace LDAP
{
   public class LDAP
    {

        // PROPS
        public PrincipalContext ctx;
        public Cryptonic.Config config;
        public List<string> whiteList;
        public List<string> blackList;
        

        // METHODS


        // Constructor
        public LDAP(Cryptonic.Config configFile, string hash, List<string> whitelist, List<string> blacklist)
        {
           
            try
            {
                // the context
                ctx = new PrincipalContext(ContextType.Domain, configFile.serverName, EncDec.Decrypt(configFile.serverPath, hash), EncDec.Decrypt(configFile.serverLogin, hash), EncDec.Decrypt(configFile.serverPassword, hash));
                
                // the config
                config = configFile;

                // white and black lists
                whiteList = whitelist;
                blackList = blacklist;
                
            }
            catch (Exception ex) {
                MessageBox.Show("Error Loading context: " + ex.Message + ". Vérifiez votre fichier de conf.", "Context ERROR");
            }
            
        }



        // log things in a file (if config debugMode is on)
        private void log(string txt, bool newline = true)
        {

            if (config.debugMode == "1")
            {
                Console.WriteLine(txt);
                StreamWriter sr = new StreamWriter("import.log", true, System.Text.Encoding.UTF8);

                if (newline)
                {
                    sr.WriteLine("");
                }

                sr.Write(txt);

                sr.Close();

            }
            
        }


        // Get all groups in the specific context (emptyGroup :  get empty nodes true/false)
        public Dictionary<string, List<Member>> getGroups()
        {

            Dictionary<string, List<Member>> groupList = new Dictionary<string, List<Member>>();

            try
            {

                // delete log file
                if (File.Exists("import.log")) File.Delete("import.log");

                // define a "query-by-example" principal - here, we search for a GroupPrincipal 
                GroupPrincipal qbeGroup = new GroupPrincipal(ctx);

                // create your principal searcher passing in the QBE principal    
                PrincipalSearcher srch = new PrincipalSearcher();
                srch.QueryFilter = qbeGroup;

                PrincipalSearchResult<Principal> result = srch.FindAll();

                log("[START] Begin GETGroups @ " + DateTime.Now);

                // find all matches
                foreach (Principal group in result)
                {
                    // only groups in whitelist (comment block if you want a more systematic check )
                    if ( whiteList.Contains(group.Name) )
                    {
                        log("[GROUP] " + group.Name + " is in WL");
                        groupList.Add(group.Name, getMembersByGroup(group.Name));
                    }
                    else
                    {
                        log("[GROUP] " + group.Name + " is not in WL");
                    }                
                }



                log("[END  ] End GETGroups (" + groupList.Count + " group.s)  @ " + DateTime.Now);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message, "Error getGroups");
                log("[ERROR] getGroups (" + ex.Message + ")");
            }

            return groupList;

        }


        // List all users of a specific group
        public List<Member> getMembersByGroup(string group)
        {
            // member list
            List<Member> memberList = new List<Member>();
            string userName = "";
            string userLogin = "";
            string userHomedir = "";


            // Get the group
            GroupPrincipal grp = GroupPrincipal.FindByIdentity(ctx, IdentityType.Name, group);

            if (grp != null)
            {

                foreach (UserPrincipal p in grp.GetMembers(true))
                {

                    log("[USER ] " + p.SamAccountName);

                    if (config.showDisabled == "1" || p.Enabled == true)
                    {

                        log(" is Enabled or show disable is on.", false);

                        try
                        {
                            userName = p.Surname + " " + p.GivenName;
                            userLogin = p.SamAccountName;
                                                       

                            UserPrincipal userPrincipal = UserPrincipal.FindByIdentity(ctx, IdentityType.Name, p.Name); // get user
                            DirectoryEntry directoryEntry = userPrincipal.GetUnderlyingObject() as DirectoryEntry; //get Homedir
                            Boolean showUser = false; // check if user is from a white listed group or if the whiteList is empty


                            // test every group of the BLACKlist (less systematic check)
                            if (blackList.Count > 0)
                            {
                                foreach (string g in blackList)
                                {
    
                                    // the user is in a blacklisted group
                                    if (userPrincipal.IsMemberOf(ctx, IdentityType.Name, g))
                                    {
                                        log(" Is in black list, HIDE user.", false);
                                        showUser = false;
                                        break;
                                    }
                                    else
                                    {
                                        log(" Is NOT in black list, SHOW user.", false);
                                        showUser = true;
                                    }

                                }

                            }

                            else
                            {
                                log(" BL is empty, SHOW user.", false);
                                showUser = true;
                            }



                            // we are using the whitelist (more systematic)
                            /*if (whiteList.Count > 0) {

                                Console.WriteLine("WL is not empty");

                                // test every group of the WHITElist
                                foreach (string g in whiteList)
                                {

                                    Console.WriteLine("testing WL group : " + g);

                                    // the user is in a whitelisted group
                                    if (userPrincipal.IsMemberOf(ctx, IdentityType.Name, g)) {
                                        showUser = true;
                                        Console.WriteLine("show user");
                                        break;
                                    }

                                }

                                // test every group of the BLACKlist
                                foreach (string g in blackList)
                                {

                                    Console.WriteLine("testing BL group : " + g);

                                    // the user is in a backlisted group
                                    if (userPrincipal.IsMemberOf(ctx, IdentityType.Name, g))
                                    {
                                        showUser = false;
                                        Console.WriteLine("hide user");
                                        break;
                                    }

                                }

                                Console.WriteLine("user state : " + showUser);

                            }

                            // we do not use the whitelist filter -> everybody
                            else
                            {
                                Console.WriteLine("WL is empty");

                                // test every group of the BLACKlist
                                foreach (string g in blackList)
                                {

                                    // the user is NOT in a blacklisted group
                                    if (!userPrincipal.IsMemberOf(ctx, IdentityType.Name, g))
                                    {
                                        showUser = true;
                                    }

                                }

                            }*/



                            // Console.WriteLine("user " + userName + " -> chown : " + showUser);



                            if (showUser)   // get only user from this group
                            {

                                if (directoryEntry.Properties.Contains("homeDirectory"))
                                {
                                    userHomedir = directoryEntry.Properties["homeDirectory"].Value.ToString();
                                    log(" Homedir: " + userHomedir, false);
                                }
                                else
                                {
                                    log(" No homedir.", false);
                                }


                                if (userHomedir != "") // Adding only users with Homedirs
                                {
                                    Member member = new Member(userName, userLogin, userHomedir);   // create a member

                                    memberList.Add(member); // add it to group
                                }

                            }


                        }
                        catch (Exception ex)
                        {
                            log("[ERROR] Error on member : " + userName + " --> " + ex.Message);
                        }


                    } // user is enabled
                    else
                    {
                        log(" is Disabled, exit !", false);
                    } // user is disabled

                } // foreach users


                grp.Dispose();
            }
            else {
                log("[WARN ] !! No group found !!");
            }

            return memberList;
        }



        // Export the treeview in a XML file
        public void exportToJSON(string filename)
        {
            Console.WriteLine("export LDAP");

            Dictionary<string, List<Member>> groups = getGroups();

            StreamWriter sr = new StreamWriter(filename, false, System.Text.Encoding.UTF8);

            // serialize all the groups        
            sr.WriteLine(JsonConvert.SerializeObject(groups));

            // close the streamwriter
            sr.Close();

        }




        // Import a JSON file in the treeview
        public void importJSON(TreeView tv, string filename, bool importEmptyNode)
        {

            try
            {
                //Just a good practice -- change the cursor to a wait cursor while the nodes populate
                tv.Cursor = Cursors.WaitCursor;

                tv.Nodes.Clear();

                // deszrialize the JSON file
                StreamReader sr = new StreamReader(filename, Encoding.UTF8);
                var groups = JsonConvert.DeserializeObject<Dictionary<string, List<Member>>>(sr.ReadToEnd());

                foreach (var group in groups) {
                    
                    List<Member> members = group.Value;

                    if (members.Count > 0 || importEmptyNode)
                    {
                        // group node
                        TreeNode node;

                        if (config.groupFilter == "")
                        {
                            node = tv.Nodes.Add(group.Key, group.Key);
                        }
                        else
                        {
                            string nodeName = group.Key.Replace(config.groupFilter, "");
                            node = tv.Nodes.Add(group.Key, nodeName);
                        }



                        // members in the group
                        foreach (var member in members)
                        {
                            TreeNode memberNode = node.Nodes.Add(member.homedir, member.name);
                        }
                    }
                    
                    
                }
                
            }
            catch (Exception ex) //General exception
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                tv.Cursor = Cursors.Default; //Change the cursor back
            }

        }

    }



    // Member class
    public class Member
    {
        public string name;
        public string ISAM;
        public string homedir;

        public Member(string member_name, string member_ISAM, string member_homedir)
        {
            name = member_name;
            ISAM = member_ISAM;
            
            if (member_homedir != "") {
                homedir = member_homedir;
            }
            
        }
    }


    

}
